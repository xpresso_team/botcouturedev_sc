
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:59
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:59
    def at(file:String): Call = {
    
      (file: @unchecked) match {
      
        // @LINE:59
        case (file) if file == "index.html" =>
          implicit val _rrc = new ReverseRouteContext(Map(("path", "/public/swagger-ui"), ("file", "index.html")))
          Call("GET", _prefix + { _defaultPrefix } + "docs/")
      
        // @LINE:60
        case (file)  =>
          implicit val _rrc = new ReverseRouteContext(Map(("path", "/public/swagger-ui")))
          Call("GET", _prefix + { _defaultPrefix } + "docs/" + implicitly[PathBindable[String]].unbind("file", file))
      
      }
    
    }
  
  }

  // @LINE:6
  class ReverseApplication(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix)
    }
  
  }


}
