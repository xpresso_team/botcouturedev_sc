
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

package ai.commerce.xpresso.v1_1.controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final ai.commerce.xpresso.v1_1.controllers.ReverseCloudSearchGet CloudSearchGet = new ai.commerce.xpresso.v1_1.controllers.ReverseCloudSearchGet(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final ai.commerce.xpresso.v1_1.controllers.javascript.ReverseCloudSearchGet CloudSearchGet = new ai.commerce.xpresso.v1_1.controllers.javascript.ReverseCloudSearchGet(RoutesPrefix.byNamePrefix());
  }

}
