
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:16
package ai.commerce.xpresso.v1_1.controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:16
  class ReverseCloudSearchGet(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def genericAPI: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1_1.controllers.CloudSearchGet.genericAPI",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1.1/catalog/genericAPI/"})
          }
        
        }
      """
    )
  
  }


}
