#! /bin/bash

PID=`cat ./target/universal/stage/RUNNING_PID`
sudo kill -9 ${PID}
echo $PID
rm ../target/universal/stage/RUNNING_PID
