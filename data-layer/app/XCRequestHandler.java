import javax.inject.Inject;

import play.Logger;
import play.routing.Router;
import play.api.mvc.Handler;
import play.http.*;
import play.mvc.*;
import play.libs.streams.Accumulator;
import play.core.j.JavaHandler;
import play.core.j.JavaHandlerComponents;

public class XCRequestHandler implements HttpRequestHandler {
    private final Router router;
    private final JavaHandlerComponents components;

    @Inject
    public XCRequestHandler(Router router, JavaHandlerComponents components) {
        this.router = router;
        this.components = components;
    }

    public HandlerForRequest handlerForRequest(Http.RequestHeader request) {
        Handler handler = router.route(request).orElseGet(() ->
                        EssentialAction.of(req -> Accumulator.done(Results.notFound()))
        );
        if (handler instanceof JavaHandler) {
            handler = ((JavaHandler)handler).withComponents(components);
        }

        Logger.info("Request from: "+ request.remoteAddress() + "-" + request.method() +" " + request.uri());

        return new HandlerForRequest(request, handler);
    }



}