import ai.commerce.xpresso.v1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1.data.DynamoDBData;
import ai.commerce.xpresso.v1.size.SizeChart;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.mashape.unirest.http.Unirest;
import play.Application;
import play.Configuration;
import play.GlobalSettings;
import play.Logger;

import java.io.IOException;

public class Global extends GlobalSettings {

    @Override
    public void onStart(Application app) {
        Configuration configuration = app.configuration();
        //Default search type
        XCConfig.SEARCH_TYPE = configuration.getString("datalayer.cloudsearch.search_type");

        //Default number of matched results
        XCConfig.MAX_SEARCH_HITS = configuration.getInt("datalayer.cloudsearch.max_search_hits");

        //If structured search hits is less than min_search_hits, it will append results from simple search
        XCConfig.MAX_SUGGESTED_HITS = configuration.getInt("datalayer.cloudsearch.max_suggested_hits");

        //If structured search hits is less than min_search_hits, it will append results from simple search
        XCConfig.MIN_SEARCH_HITS = configuration.getInt("datalayer.cloudsearch.min_search_hits");

        XCConfig.ENABLE_STANDARD_SIZE = configuration.getBoolean("datalayer.enable_standard_size",false);

        // Init Size
        SizeChart.getInstance(configuration);

//        // Init Dynamodb
//        DynamoDBData.getInstance(configuration)
//                .setTable(configuration.getString("datalayer.dynamodb.default_table_name"));
//        DynamoDBData.getInstance().init();

        Logger.info("Application has started");
    }

    @Override
    public void onStop(Application app) {
        Logger.info("Application shutdown...");

        // Unirest client needs to be shutdown separately
        // as it keeps a thread open always.
//        try {
//            Unirest.shutdown();
//        } catch (IOException e) {
//            Logger.info("Unirest not shut down");
//        }
    }

}