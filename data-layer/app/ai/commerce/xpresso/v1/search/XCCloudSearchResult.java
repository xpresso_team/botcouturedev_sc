package ai.commerce.xpresso.v1.search;

import ai.commerce.xpresso.v1.amazon.cloudsearch.Hit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by naveen on 23/5/17.
 *
 * Stores the result from cloudsearch and
 * coressponding meta data for each request
 */
public class XCCloudSearchResult {

    /*
     * Stores list of results from the cloudsearach
     */
    public List<SearchItem> mResultList = new ArrayList<SearchItem>();

    /*
     *  Faceted results
     */
    public Map<String, List<String>> mFacetMaps = new HashMap<>();

    /**
     * Process time from cloudsearch
     */
    public long mTime = 0 ;

    /**
     * Number of results found
     */
    public int mFound = 0;

    /**
     * Start position. Mostly defined to 0
     */
    public int mStart = 0 ;


    /**
     * Cursor string to iterate the data.
     */
    public String mCursor ="";


    public XCCloudSearchResult(List<SearchItem> resultList,
                               int found,
                               long time,
                               int start,
                               String cursor,
                               Map<String, List<String>> facetMap){
        mResultList = resultList;
        mFound = found;
        mTime = time;
        mStart = start;
        mCursor = cursor;
        mFacetMaps = facetMap;
    }

    /**
     * Placeholder constructor for empty results
     */
    public XCCloudSearchResult(){
    }

    /**
     * Get resultItems
     */

    public  List<SearchItem> getResults(){
        return mResultList;
    }

    /**
     * get facet results
     */
    public Map<String, List<String>> getFacetMaps(){ return mFacetMaps;}

}
