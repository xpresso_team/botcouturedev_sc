package ai.commerce.xpresso.v1.amazon.dynamodb;

import ai.commerce.xpresso.v1.amazon.cloudsearch.DomainCategoryMapper;
import ai.commerce.xpresso.v1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1.utils.ErrorHandler;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.*;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.util.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * @author abzooba
 *
 */

public class AmazonDynamoDB {

	public DynamoDB mDynamoDB = null;
	public AmazonDynamoDBClient mAmazonDynamoDBClient = null;

	public static String FAIL_STATUS = "fail";

	/**
	 * Used in updating cloud search index.
	 * @param domainName table name of the data
	 * @return - JSONArray of items fetched from mDynamoDB with attributes defined the function
	 */
	public JSONArray retrieveAll(String domainName) {
		Map<String, AttributeValue> lastEvaluatedKey = null;
		int counter = 0;
		JSONArray ja = new JSONArray();

		while (XCConfig.maxReviewCount > 0 && counter < XCConfig.maxReviewCount) {
			ScanRequest scanRequest = new ScanRequest().withTableName(domainName);
			if (lastEvaluatedKey != null)
				scanRequest.setExclusiveStartKey(lastEvaluatedKey);
			else
				play.Logger.info("last evaluated key is null");

			ScanResult result = mAmazonDynamoDBClient.scan(scanRequest);

			if (result == null) {
				play.Logger.info("Scan result is null");
				return ja;
			}

			lastEvaluatedKey = result.getLastEvaluatedKey();
			for (Map<String, AttributeValue> item : result.getItems()) {
				if (counter >= XCConfig.maxReviewCount)
					break;
				play.Logger.info("counter - " + counter++);
				JSONObject mainObj = new JSONObject();
				try {
					AttributeValue attribute = item.get("category");
					mainObj.put("category", (attribute != null) ? attribute.getSS() : "default");

					attribute = item.get("image");
					mainObj.put("image", (attribute != null) ? attribute.getS() : "default");

					attribute = item.get("sku");
					mainObj.put("sku", (attribute != null) ? attribute.getS() : "default");

					attribute = item.get("gender");
					mainObj.put("gender", (attribute != null) ? attribute.getS() : "default");

					attribute = item.get("price");
					String tempprice = (attribute != null) ? attribute.getS() : "0";
					tempprice = tempprice.trim().replaceAll("[^0-9\\.]+", "");
					play.Logger.info("tempprice before parsing : " + tempprice);
					mainObj.put("price", (!tempprice.isEmpty() && !tempprice.equals("")) ? Double.parseDouble(tempprice) : 0);

					attribute = item.get("subcategory");
					mainObj.put("subcategory", (attribute != null) ? attribute.getSS() : "default");

					attribute = item.get("brand");
					mainObj.put("brand", (attribute != null) ? attribute.getS() : "default");

					attribute = item.get("productname");
					mainObj.put("productname", (attribute != null) ? attribute.getS() : "default");

					attribute = item.get("details");
					mainObj.put("details", (attribute != null) ? attribute.getS().replaceAll("[^\\w\\s\\-_]", "") : "default");

					attribute = item.get("colorsavailable");
					mainObj.put("colorsavailable", (attribute != null) ? attribute.getSS() : "[default]");

					attribute = item.get("colorsgroup");
					mainObj.put("colorsgroup", (attribute != null) ? attribute.getSS() : "[default]");

					attribute = item.get("department");
					mainObj.put("department", (attribute != null) ? attribute.getSS() : "[default]");

					attribute = item.get("keywords");
					mainObj.put("keywords", (attribute != null) ? attribute.getS() : "default");

					attribute = item.get("size");
					mainObj.put("size", (attribute != null) ? attribute.getSS() : "[default]");

					attribute = item.get("features");
					mainObj.put("features", (attribute != null) ? attribute.getS().replaceAll("[^\\w\\s\\-_]", "") : "default");

					ja.put(mainObj);
				} catch (JSONException | NumberFormatException e) {
					Logger.info(e.getMessage());
				}
			}
		}
		play.Logger.info("Items retrieved " + counter);
		return ja;
	}

	/**
	 * 
	 * @param domainName
	 * @param gender
	 * @param category
	 * @param subcategory
	 * @param section
	 * @return - Items with given filters
	 */
	public List<?> retrieveAll(String domainName, String gender, String category, String subcategory, String section) {
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
		Map<String, String> expressionAttributeNames = new HashMap<String, String>();
		expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
		expressionAttributeValues.put(":category", new AttributeValue().withS(category));
		expressionAttributeValues.put(":subcategory", new AttributeValue().withS(subcategory));
		expressionAttributeValues.put(":subsubcategory", new AttributeValue().withS(section));
		expressionAttributeNames.put("#s", "Section");
		ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
		scanRequest.withFilterExpression("gender = :gender AND category = :category AND subcategory = :subcategory AND #s = :subsubcategory");
		scanRequest.withExpressionAttributeValues(expressionAttributeValues);
		scanRequest.withExpressionAttributeNames(expressionAttributeNames);
		scanRequest.withProjectionExpression("productname,brand,price,image,sku");
		ScanResult result = mAmazonDynamoDBClient.scan(scanRequest);
		List<?> resultItems = result.getItems();
		return resultItems;
	}

	/**
	 * @param name
	 * @return
	 * @throws FileNotFoundException
	 */
	private InputStream findStreamInClasspathOrFileSystem(String name) throws FileNotFoundException {
		try {
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
			if (is == null)
				is = new FileInputStream(name);
			if (name.endsWith(".gz")) {
				try {
					return new GZIPInputStream(is);
				} catch (IOException e) {
					play.Logger.error("Resource or file looks like a gzip file, but is not: " + name);
				}
			}
			return is;
		} catch (Exception ex) {
			InputStream is = IOUtils.class.getClassLoader().getResourceAsStream(name);
			if (is == null) {
				is = IOUtils.class.getClassLoader().getResourceAsStream(name.replaceAll("\\\\", "/"));
				if (is == null) {
					is = IOUtils.class.getClassLoader().getResourceAsStream(name.replaceAll("\\\\", "/").replaceAll("/+", "/"));
				}
			}
			if (is == null)
				is = new FileInputStream(name);
			if (name.endsWith(".gz")) {
				try {
					return new GZIPInputStream(is);
				} catch (IOException e) {
					play.Logger.error("Resource or file looks like a gzip file, but is not: " + name);
				}
			}
			return is;
		}
	}

	/**
	 * @param fileName
	 * @return
	 */
	public InputStream getInputStreamFromFileName(String fileName) {
		InputStream is = null;
		is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
		if (is != null) {
			return is;
		}
		try {
			is = findStreamInClasspathOrFileSystem(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return is;
	}

	/**
	 * @param is
	 * @return
	 */
	public String convertStreamToString(InputStream is) {
		String result = null;
		try (Scanner s = new Scanner(is)) {
			s.useDelimiter("\\A");
			result = s.hasNext() ? s.next() : "";
		} catch (Exception e) {
			play.Logger.error(e.getMessage());
		}
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param word
	 * @return - True if "source" contains "word", otherwise false 
	 */
	public boolean isContain(String source, String word) {
		String pattern = "\\b" + word.toLowerCase() + "\\b";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(source.toLowerCase());
		return m.find();
	}

	/**
	 * Displays list of tables in mDynamoDB to the console
	 */
	public void listTable() {
		TableCollection<ListTablesResult> tables = mDynamoDB.listTables();
		Iterator<Table> iterator = tables.iterator();
		while (iterator.hasNext()) {
			Table table = iterator.next();
			play.Logger.info(table.getTableName());
		}
	}

	public JSONObject deleteProduct(String tableName, String keyName, String[] keyValue ){
		Table table = null;
		try {
			table = mDynamoDB.getTable(tableName);
		}catch(IllegalArgumentException e){
			play.Logger.info(e.getMessage());
			table = null;
		}
		if( table == null ){
			return ErrorHandler.generateError(400,"fail","Invlid table");
		}

		// Conditional delete (we expect this to fail)
		try {
			for( int i = 0 ; i < keyValue.length ; ++i) {
				DeleteItemSpec deleteItemSpec = new DeleteItemSpec()
						.withPrimaryKey(keyName, keyValue[i]);
				table.deleteItem(deleteItemSpec);
			}
		}
		catch (Exception e) {
			return ErrorHandler.generateError(400,"fail","Invlid key values");
		}

		JSONObject finalResults = new JSONObject();
		try {
			finalResults.put("status","ok");
			finalResults.put("code",200);
			finalResults.put("msg", "ok");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return finalResults;
	}

	public JSONObject insertProduct(String tableName, String keyName, JSONArray items) {
		Table table = null;
		try {
			table = mDynamoDB.getTable(tableName);
		}catch(IllegalArgumentException e){
			play.Logger.info(e.getMessage());
			table = null;
		}
		if( table == null ){
			return ErrorHandler
					.generateError(400,"fail","Invalid Table Name");
		}
		JSONObject finalResults = new JSONObject();

		for ( int i = 0; i < items.length() ; ++i) {
			try {
				JSONObject item = items.getJSONObject(i);
				final Map<String, Object> infoMap = new HashMap<String, Object>();
				Item dbItem = new Item().withPrimaryKey(keyName, item.getString(keyName));

				Iterator<?> keys = item.keys();
				while( keys.hasNext() ) {
					String key = (String)keys.next();
					JSONObject valueJson = (JSONObject) item.get(key);
					String type = valueJson.getString("type");

					// Convert json data to dynamodb specific data
					if( valueJson.getString("type").equalsIgnoreCase("N")){
						dbItem.withDouble(key,valueJson.getDouble("value") );
					}else if( valueJson.getString("type").equalsIgnoreCase("B")){
						dbItem.withBoolean(key,valueJson.getBoolean("value") );
					}else if( valueJson.getString("type").equalsIgnoreCase("SS")){
						Set<String> stringSet = new HashSet<String>();
						JSONArray jArray = valueJson.getJSONArray("value");
						if (jArray != null) {
							for (int j=0;j<jArray.length();++j){
								stringSet.add(jArray.getString(j));
							}
						}
						dbItem.withStringSet(key, stringSet );
					}else if( valueJson.getString("type").equalsIgnoreCase("L")){
						List<String> stringList = new ArrayList<>();
						JSONArray jArray = valueJson.getJSONArray("value");
						if (jArray != null) {
							for (int j=0;j<jArray.length();++j){
								stringList.add(jArray.getString(j));
							}
						}
						dbItem.withList(key, stringList );
					}else{
						dbItem.withString(key,valueJson.getString("value"));
					}
				}
				PutItemOutcome outcome = table.putItem(dbItem);
				Logger.info("PutItem succeeded:\n" + outcome.getPutItemResult());

			} catch (JSONException e) {
				Logger.error(e.getMessage());
				return ErrorHandler
						.generateError(400,"fail","Invalid Data");
			}
		}
		return finalResults;
	}

	public JSONObject getProduct(String tableName, String keyName, String keyValue){
		Table table = null;
		try {
			table = mDynamoDB.getTable(tableName);
		}catch(IllegalArgumentException e){
			play.Logger.info(e.getMessage());
			table = null;
		}
		if( table == null ){
			return ErrorHandler
					.generateError(400,"fail","Invalid Table Name");
		}

		GetItemSpec spec = new GetItemSpec().withPrimaryKey(keyName, keyValue);

		// Fetch item if exist otherwise send error msg
		boolean ifItemExist = false;
		JSONObject finalResults = new JSONObject();
		try {
			Item result = table.getItem(spec);
			finalResults = new JSONObject(result.toJSON());
			ifItemExist = true;
		}catch (Exception e){
			ifItemExist = false;
			play.Logger.info(e.getMessage());
		}

		if (!ifItemExist) {
			JSONObject errorMsg = new JSONObject();
			try {
				errorMsg.put("status",FAIL_STATUS);
				errorMsg.put("msg","key does not exist");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return errorMsg;

		}
		return finalResults;
	}

	public JSONObject updateProduct(String tableName,
									String keyName,
									String keyValue,
									JSONObject updateItem) {

		Table table = null;
		try {
			table = mDynamoDB.getTable(tableName);
		} catch (IllegalArgumentException e) {
			Logger.info(e.getMessage());
			table = null;
			return ErrorHandler
					.generateError(400, "fail", "Invalid Table Name");
		}

		// Conditional update (we expect this to fail)
		JSONObject outPut = new JSONObject();
		try {
			UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey(keyName, keyValue)
					.withReturnValues(ReturnValue.UPDATED_NEW);

			StringBuilder updateExp = new StringBuilder("set ");

			ValueMap valueMap = new ValueMap();

			Iterator<?> keys = updateItem.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				JSONObject valueJson = (JSONObject) updateItem.get(key);
				String type = valueJson.getString("type");

				// Convert json data to dynamodb specific data
				if (valueJson.getString("type").equalsIgnoreCase("N")) {
					valueMap.with(":" + key, valueJson.getDouble("value"));
				} else if (valueJson.getString("type").equalsIgnoreCase("B")) {
					valueMap.with(":" + key, valueJson.getBoolean("value"));
				} else if (valueJson.getString("type").equalsIgnoreCase("SS")) {
					Set<String> stringSet = new HashSet<String>();
					JSONArray jArray = valueJson.getJSONArray("value");
					if (jArray != null) {
						for (int j = 0; j < jArray.length(); ++j) {
							stringSet.add(jArray.getString(j));
						}
					}
					valueMap.with(":" + key, stringSet);
				} else if (valueJson.getString("type").equalsIgnoreCase("L")) {
					List<String> stringList = new ArrayList<>();
					JSONArray jArray = valueJson.getJSONArray("value");
					if (jArray != null) {
						for (int j = 0; j < jArray.length(); ++j) {
							stringList.add(jArray.getString(j));
						}
					}
					valueMap.with(":" + key, stringList);
				} else {
					valueMap.with(":" + key, valueJson.getString("value"));
				}

				updateExp.append(key)
						.append(" = ")
						.append(":")
						.append(key)
						.append(", ");
			}
			String updateExperssionStr = updateExp.toString();
			updateExperssionStr = updateExperssionStr
					.substring(0, updateExperssionStr.length() - 2);
			Logger.info("DynamoDBData Expression: " + updateExperssionStr);
			updateItemSpec.withUpdateExpression(updateExperssionStr)
					.withValueMap(valueMap);
			UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
			outPut = new JSONObject(outcome.getItem().toJSON());

		} catch (Exception e) {
			Logger.info(e.getMessage());
			return ErrorHandler
					.generateError(400, "fail", "update failed");
		}


		JSONObject finalResults = new JSONObject();
		try {
			finalResults.put("Results", outPut);
			finalResults.put("status", "ok");
			finalResults.put("msg", "ok");
		} catch (JSONException e) {
			Logger.info(e.getMessage());
		}
		return finalResults;
	}


	public JSONObject scanAll(String tableName, List<String> returnFields){
		Table table = null;
		try {
			table = mDynamoDB.getTable(tableName);
		} catch (IllegalArgumentException e) {
			Logger.info(e.getMessage());
			table = null;
			return ErrorHandler
					.generateError(400, "fail", "Invalid Table Name");
		}
		// To ensure easy scan of complete data
		// Increase the table throughput
		UpdateTableSpec updateTableSpecInc = new UpdateTableSpec();
		updateTableSpecInc.withProvisionedThroughput(
				new ProvisionedThroughput().withReadCapacityUnits(1000L)
						.withWriteCapacityUnits(5L));
		try {
			table.updateTable(updateTableSpecInc);
		}catch (AmazonDynamoDBException e){
			Logger.error(e.getMessage());
		}



		ScanSpec scanSpec = new ScanSpec();
		scanSpec.withFilterExpression("is_available = :is_available AND xc_category <> :xc_category")
				.withValueMap(new ValueMap()
						.withBoolean(":is_available", true)
						.withStringSet(":xc_category", "others"));


		if (!returnFields.isEmpty()) {
			scanSpec.withAttributesToGet(String.valueOf(returnFields));
		}

//		scanSpec.withMaxResultSize(100);

        JSONArray results = new JSONArray();
		try {
			ItemCollection<ScanOutcome> items = table.scan(scanSpec);
			Iterator<Item> iter = items.iterator();
			while (iter.hasNext()) {
				Item item = iter.next();
				results.put(new JSONObject(item.toJSON()));
			}

		} catch (Exception e) {
			Logger.error("Unable to scan the table:");
			Logger.error(e.getMessage());
		}

		// Decrease the throughput after update
		// We have to make sure this has been completed.
		// Retrying for three times
		int retryAttempts = 1;
		while( retryAttempts-- > 0) {
			try {
				UpdateTableSpec updateTableSpecDec = new UpdateTableSpec();
				updateTableSpecDec.withProvisionedThroughput(
						new ProvisionedThroughput().withReadCapacityUnits(5L).
								withWriteCapacityUnits(5L));
				table.updateTable(updateTableSpecDec);
				retryAttempts=0;
			} catch (AmazonDynamoDBException e) {
				Logger.error(e.getMessage());
				// Wait for 2 sec before retrying
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		JSONObject finalResults = new JSONObject();
		try {
			finalResults.put("Results", results);
			finalResults.put("ResultsCount", results.length());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return finalResults;
	}

	public AmazonDynamoDB(Configuration configuration){

		// Setup general configuration for the HTTPS connection
		ClientConfiguration clientConfiguration = new ClientConfiguration();
		clientConfiguration.setProtocol(Protocol.HTTPS);
		clientConfiguration.setMaxErrorRetry(2);
		clientConfiguration.setConnectionTimeout(5 * 60 * 1000);
		clientConfiguration.setClientExecutionTimeout(5 * 60 * 1000);
		clientConfiguration.setSocketTimeout(5 * 60 * 1000);
		clientConfiguration.setRequestTimeout(5 * 60 * 1000);

        AWSCredentials awsCredentials =new BasicAWSCredentials(
                configuration.getString("datalayer.dynamodb.aws_access_key"),
                configuration.getString("datalayer.dynamodb.aws_secret_key"));


        mAmazonDynamoDBClient = new AmazonDynamoDBClient(awsCredentials, clientConfiguration);
		mAmazonDynamoDBClient.setRegion(Region.getRegion(
                Regions.fromName(configuration.getString("datalayer.dynamodb.default_region"))));
		mDynamoDB = new DynamoDB(mAmazonDynamoDBClient);
		Logger.info("DynamoDBData Connected");
	}
}
