package ai.commerce.xpresso.v1.amazon.dynamodb;

import ai.commerce.xpresso.v1.amazon.cloudsearch.DomainCategoryMapper;
import ai.commerce.xpresso.v1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1.utils.ErrorHandler;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.*;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.util.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * @author abzooba
 *
 * DynamoDB client to handle collections database
 */

public class AmazonCollectionsDynamoDB {

	public DynamoDB mDynamoDB = null;
	public AmazonDynamoDBClient mAmazonDynamoDBClient = null;

	public static String FAIL_STATUS = "fail";



	public AmazonCollectionsDynamoDB(Configuration configuration){

		// Setup general configuration for the HTTPS connection
		ClientConfiguration clientConfiguration = new ClientConfiguration();
		clientConfiguration.setProtocol(Protocol.HTTPS);
		clientConfiguration.setMaxErrorRetry(2);
		clientConfiguration.setConnectionTimeout(5 * 60 * 1000);
		clientConfiguration.setClientExecutionTimeout(5 * 60 * 1000);
		clientConfiguration.setSocketTimeout(5 * 60 * 1000);
		clientConfiguration.setRequestTimeout(5 * 60 * 1000);

        AWSCredentials awsCredentials =new BasicAWSCredentials(
                configuration.getString("datalayer.dynamodb.aws_access_key"),
                configuration.getString("datalayer.dynamodb.aws_secret_key"));


        mAmazonDynamoDBClient = new AmazonDynamoDBClient(awsCredentials, clientConfiguration);
		mAmazonDynamoDBClient.setRegion(Region.getRegion(
                Regions.fromName(configuration.getString("datalayer.dynamodb.default_region"))));
		mDynamoDB = new DynamoDB(mAmazonDynamoDBClient);
		Logger.info("DynamoDBData Connected");
	}
}
