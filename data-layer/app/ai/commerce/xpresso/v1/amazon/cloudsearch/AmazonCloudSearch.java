package ai.commerce.xpresso.v1.amazon.cloudsearch;

import ai.commerce.xpresso.v1.search.SearchAttributes;
import ai.commerce.xpresso.v1.search.SearchController;
import ai.commerce.xpresso.v1.search.SearchItem;
import ai.commerce.xpresso.v1.search.XCCloudSearchResult;
import ai.commerce.xpresso.v1.utils.XCEntity;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudsearchv2.model.DescribeDomainsRequest;
import com.amazonaws.services.cloudsearchv2.model.DescribeDomainsResult;
import com.amazonaws.services.cloudsearchv2.model.DomainStatus;
import com.amazonaws.services.cloudsearchv2.model.ServiceEndpoint;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;

import java.util.*;

/**
 * Created by naveen on 10/5/17.
 */
public class AmazonCloudSearch {

    private Configuration mConfiguration;

    private AmazonCloudSearchClientCustom mCloudSearchClient;


    /**
     * Setup initial configuratino of cloudsearch.
     * Endpoints are setup automatically by domain names.
     * @param configuration
     */
    public AmazonCloudSearch(Configuration configuration){

        // Setup general configuration for the HTTPS connection
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setProtocol(Protocol.HTTPS);
        clientConfiguration.setMaxErrorRetry(2);
        clientConfiguration.setConnectionTimeout(5 * 60 * 1000);
        clientConfiguration.setClientExecutionTimeout(5 * 60 * 1000);
        clientConfiguration.setSocketTimeout(5 * 60 * 1000);
        clientConfiguration.setRequestTimeout(5 * 60 * 1000);

        AWSCredentials awsCredentials =new BasicAWSCredentials(
                configuration.getString("datalayer.cloudsearch.aws_access_key"),
                configuration.getString("datalayer.cloudsearch.aws_secret_key"));

        // Instantiate the data
        mCloudSearchClient = new AmazonCloudSearchClientCustom(awsCredentials, clientConfiguration);
        mCloudSearchClient.setRegion(Region.getRegion(
                Regions.fromName(configuration.getString("datalayer.cloudsearch.default_region"))));

        Logger.info("Setting endpoints");
        setEndpoints(configuration.getString("datalayer.cloudsearch.default_domain"));

        SearchAttributes.init();
        mConfiguration=configuration;

    }


    /**
     * Fetches the data of domain from aws and sets the required endpoints
     * @param domain
     */
    private void setEndpoints(String domain){
        // Fetch the endpoints for the default cloudsearch
        DescribeDomainsRequest describeDomainRequest = new DescribeDomainsRequest();
        describeDomainRequest.withDomainNames(domain);
        DescribeDomainsResult domainResult = mCloudSearchClient.describeDomains(describeDomainRequest);
        List<DomainStatus> domainStatuses = domainResult.getDomainStatusList();
        if(domainStatuses.size() > 0){
            DomainStatus domainStatus = domainStatuses.get(0);
            ServiceEndpoint docEndpoint = domainStatus.getDocService();
            ServiceEndpoint searchEndpoint = domainStatus.getSearchService();
            mCloudSearchClient.setDocumentEndpoint(docEndpoint.getEndpoint());
            mCloudSearchClient.setSearchEndpoint(searchEndpoint.getEndpoint());
            Logger.info("["+domain+"]["+docEndpoint.getEndpoint()+"]");
            Logger.info("["+domain+"]["+searchEndpoint.getEndpoint()+"]");
        }
    }

    /**
     *  Deletes the list of documents from cloudsearch.
     *
     * @param deleteItems
     * @return returns True if delete is successful, Else returns false
     */
    public boolean deleteItems(List<String> deleteItems){
        boolean ret = false;

        List<AmazonCloudSearchDeleteRequest> deleteRequests = new ArrayList<>();
        for(String item: deleteItems){
            AmazonCloudSearchDeleteRequest request = new AmazonCloudSearchDeleteRequest();
            request.id = item;
            deleteRequests.add(request);
        }
        try {
            mCloudSearchClient.deleteDocuments(deleteRequests);
            ret= true;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (AmazonCloudSearchRequestException e) {
            e.printStackTrace();
        } catch (AmazonCloudSearchInternalServerException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Insert the given document in json format into the cloudsearch
     *
     * JSONArray should be valid otherwise insertion will fail
     * @param items JSONArray of documents in json format which needs to be updated
     * @return true if insertion is success.
     */
    public boolean insertItems(JSONArray items){
        boolean ret = false;

        List<AmazonCloudSearchAddRequest> addRequests = new ArrayList<>();
        for(int i = 0 ; i <  items.length(); ++i){
            JSONObject item = null;
            try {
                item = items.getJSONObject(i);
            } catch (JSONException e) {
                return false;
            }
            AmazonCloudSearchAddRequest request = new AmazonCloudSearchAddRequest();
            Iterator keys = item.keys();
            while( keys.hasNext()){
                String key = (String) keys.next();
                try {
                    request.addField(key,item.get(key));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            mCloudSearchClient.addDocuments(addRequests);
            ret= true;
        } catch (JSONException |
                AmazonCloudSearchRequestException |
                AmazonCloudSearchInternalServerException e) {
            Logger.error(e.getMessage());
        }

        return ret;
    }

    public XCCloudSearchResult getSearchItems(String[] returnField,
                                              String cloudSearchDomain,
                                              Map<String, Set<String>> filterMap) {

        return getSearchItems(returnField,
                cloudSearchDomain,
                filterMap,
                mConfiguration.getInt("datalayer.cloudsearch.max_search_hits"),
                "initial",
                false);
    }

    /**
     * Perform search on cloudsearch using the endpoint APIs.
     *
     * @param returnField Fields which needs to be returned from cloudsearch
     * @param cloudSearchDomain name of the domain of cloudsearch
     * @param filterMap list of filters to apply on cloudsearch
     * @return List of results hit by the search operation.
     */
    public XCCloudSearchResult getSearchItems(String[] returnField,
                                              String cloudSearchDomain,
                                              Map<String, Set<String>> filterMap,
                                              int pageSize,
                                              String cursor,
                                              boolean isFaceted) {

        SearchAttributes attribute = new SearchAttributes("", filterMap);
        SearchController objSearchController = new SearchController();

        AmazonCloudSearchQuery query = objSearchController.buildQuery(
                cloudSearchDomain,
                "structured",
                false,
                cursor,
                isFaceted,
                returnField);

        query.setSize(pageSize);

        XCEntity xcEntity = new XCEntity("", null, false);
        query.structuredQuery = attribute.queryComposition(xcEntity,
                "and", null);
        Logger.info(query.structuredQuery);

        //List to store results from cloudsearch
        List<SearchItem> resultList = new ArrayList<SearchItem>();

        AmazonCloudSearchResult result = mCloudSearchClient.getSearch(query);
        if( result == null){
            return  new XCCloudSearchResult();
        }
        List<Hit> resultHits = result.hits;

        if (resultHits != null) {
            for (Object currResult : resultHits) {
                SearchItem item = null;
                if (currResult instanceof Hit) {
                    item = new SearchItem((Hit) currResult);
                    resultList.add(item);
                }
            }
        }
        XCCloudSearchResult finalResult = new XCCloudSearchResult(resultList,
                result.found,
                result.time,
                result.start,
                result.cursor,
                result.facets);

        return finalResult;
    }


    /**
     * Perform strucured search on the cloudsearch
     *
     * @param entity to search
     * @param returnField
     * @param cloudSearchDomain
     * @param filterMap
     * @param prefixList
     * @return List of hit items
     */
    public List<SearchItem> getStructuredSearchItems(
            String entity,
            String[] returnField,
            String cloudSearchDomain,
            Map<String, Set<String>> filterMap,
            List<String> prefixList) {

        SearchController objSearchController = new SearchController();


        AmazonCloudSearchQuery query = objSearchController.buildQuery(
                cloudSearchDomain,
                "structured",
                false,
                null,
                false,
                returnField);

        XCEntity xcEntity = new XCEntity(entity, null, false);
        SearchAttributes attribute = new SearchAttributes(entity, filterMap,prefixList);
        query.structuredQuery = attribute.queryComposition( xcEntity,
                                                        "and",
                                                        null);
        Logger.info(query.structuredQuery);

        //List to store results from cloudsearch
        List<SearchItem> resultList = new ArrayList<SearchItem>();

        AmazonCloudSearchResult result = mCloudSearchClient.getSearch(query);

        if( result == null){
            return resultList;
        }
        List<Hit> resultHits = result.hits;

        if (resultHits != null) {
            for (Object currResult : resultHits) {
                SearchItem item = null;
                if (currResult instanceof Hit) {
                    item = new SearchItem((Hit) currResult);
                    resultList.add(item);
                }
            }
        }


        return resultList;
    }
}
