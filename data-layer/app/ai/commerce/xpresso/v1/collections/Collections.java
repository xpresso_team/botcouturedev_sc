package ai.commerce.xpresso.v1.collections;

import ai.commerce.xpresso.v1.amazon.dynamodb.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import play.Configuration;
import play.Logger;

import java.util.*;

/**
 * Created by Naveen on 17/6/17.
 *
 * Singleton class which holds a connection
 * collections database.
 */
public class Collections {
    /**
     * Private static instance
     */
    static private Collections mInstance = null;

    /**
     * Amazon db instance
     */
    private AmazonDynamoDB mAmazonDynamoDB = null;

    /**
     * Collections table
     */
    Table mCollectionsTable = null;

    /**
     * Application configuration
     */
    private Configuration mConfiguration = null;

    private Collections(Configuration configuration) {
        mConfiguration = configuration;
    }

    public static Collections getInstance(Configuration configuration){
        if( mInstance==null){
            mInstance = new Collections(configuration);
            mInstance.init(configuration);
        }
        return mInstance;
    }

    public static Collections getInstance(){
        return mInstance;
    }

    /**
     * Initialize the dynamodb client
     * @param configuration
     */
    private void init(Configuration configuration) {
        mAmazonDynamoDB = new AmazonDynamoDB(configuration);
        String tableName = configuration.getString("datalayer.dynamodb.default_collections_table");

        try {
            mCollectionsTable = mAmazonDynamoDB.mDynamoDB.getTable(tableName);
        } catch (IllegalArgumentException e) {
            Logger.info(e.getMessage());
            mCollectionsTable = null;
        }

        if ( mCollectionsTable == null){
            // Nothing can be done here. Size chart wont work.
            // Raise warning and exit.
            Logger.warn("Collections can not be retrieved using tableName ["+tableName+"]");
            return;
        }
    }

    /**
     * Get list of collections and trends from the db
     */
    public ArrayList<String> getCollectionList(HashMap<String, String> query, String returnField){
//        if(mCollectionsTable == null){
//            return new ArrayList<>();
//        }
//
//        ScanSpec scanSpec = new ScanSpec();
//
//        StringBuilder sb = new StringBuilder();
//        for( String entry : query.keySet()){
//            sb.append(entry)
//                    .append( " = ")
//                    .append(":")
//                    .append(entry);
//        }
//        scanSpec.withFilterExpression("is_available  = :is_available AND xc_category <> :xc_category")
//
//        for(Map.Entry<String, String> entry: query.entrySet()) {
//
//        }
//                .withValueMap(new ValueMap()
//                        .withString(":gender", gender)
//                        .withString(":type", type));
//
//        ArrayList<String> results = new ArrayList<>();
//        try {
//            ItemCollection<ScanOutcome> items = mCollectionsTable.scan(scanSpec);
//            Iterator<Item> iter = items.iterator();
//            while (iter.hasNext()) {
//                Item item = iter.next();
//                JSONObject jItem = new JSONObject(item.toJSON());
//                if( jItem.has(returnField )) {
//                    results.add(jItem.getString(returnField));
//                }
//            }
//
//        } catch (Exception e) {
//            Logger.error("Unable to scan the table:");
//            Logger.error(e.getMessage());
//        }
//
//        List<?> resultItems = result.getItems();
        return new ArrayList<>();
    }

}
