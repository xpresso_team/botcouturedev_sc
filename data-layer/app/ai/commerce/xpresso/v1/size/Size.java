package ai.commerce.xpresso.v1.size;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by naveen on 9/6/17.
 *
 * Contains single row element of a size chart
 *
 * If apparel -  contains size_letter,us no,waist, bust, hip
 * footwear  - conatins size_letter, us no, uk no, eu no
 */
public class Size {


    /**
     * List of all integer fields in size chart.
     * Order is important here. Size is matched in same order as initialized here
     */
    public static final List<String> mSizeFields = new ArrayList<>(
            Arrays.asList("us_size_no","waist","bust","hip","length","uk_size_no","eu_size_no"));

    /**
     * Name of the brand. Size are brand specific
     */
    public String mBrand = "";

    /**
     * Name of the size category like dress, shirt etc
     *
     * Now we have two generic category apparel and footwear
     */
    public String mXCCategory = "";

    /**
     * Unique Id of the size
     */
    public String mSizeId = "";


    /**
     * Standardized letter size
     */
    public String mSizeLetter = "";


    /**
     * Gender of the size
     */
    public String mGender = "";

    /**
     *  Size data list of sizes
     */
    JSONObject mData =  new JSONObject();



    public Size(JSONObject data){
        mData = data;
        try {
            mBrand = data.getString("brand");
            mXCCategory = data.getString("xc_category");
            mSizeLetter = data.getString("size_letter");
            mGender = data.getString("gender");
            mSizeId = data.getString("size_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static String makeKey(String gender, String xcCategory, String sizeLetter, String brand){
        return gender+"_"+xcCategory+"_"+brand;
    }

    /**
     * Matches two sizes with the maximum count
     * @param size
     * @return
     */
    public int getMatchCount(String size) {

        String ownSize = mSizeLetter;

        int matchCount = 0;
        for( matchCount = 0;  matchCount< Math.min(ownSize.length(),size.length()); ++matchCount){
            if(ownSize.charAt(matchCount) != size.charAt(matchCount)){
                break;
            }
        }
        Logger.info(ownSize+"-"+size+"-"+matchCount);

        // Now match expanded count
        String expandedSize = expandSize(size);
        String expandedOwnSize = expandSize(ownSize);
        int expandedMatchCount = 0;
        for( expandedMatchCount = 0;
             expandedMatchCount < Math.min(expandedSize.length(),expandedOwnSize.length());
             ++expandedMatchCount){
            if(expandedSize.charAt(expandedMatchCount) != expandedOwnSize.charAt(expandedMatchCount)){
                break;
            }
        }
        Logger.info(expandedOwnSize+"-"+expandedSize+"-"+expandedMatchCount);

        return Math.max(matchCount, expandedMatchCount);
    }

    /*
     * Now expand a nd check. To check for 6xl and 6xs type sizes
     */
    private String expandSize(String size){
        String returnSize = "";
        if( size.length() >= 2 &&
                Character.isDigit(size.charAt(0)) &&
                Character.isLetter(size.charAt(1))){
            char c = size.charAt(1);
            int repetitionCount = Integer.parseInt(size.charAt(0)+"");
            returnSize = size.substring(2);
            returnSize = c + returnSize;
        }else{
            returnSize = size;
        }
        return returnSize;
    };
}
