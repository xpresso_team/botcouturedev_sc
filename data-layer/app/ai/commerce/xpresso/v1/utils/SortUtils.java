package ai.commerce.xpresso.v1.utils;

import java.util.*;

/**
 * Utility class that is used to sort different result attributes
 */
public class SortUtils {

    /**
     * Keeps track of size score
     */
    public  static final Map<String, Double > mSizeScore = new HashMap<>();

    /**
     * Supported Attributes
     */
    public static enum SORT_TYPE{
        GENERIC,
        SIZE
    }

    static{
        mSizeScore.put("xxxx-small", 100.0);
        mSizeScore.put("xxx-small", 101.0);
        mSizeScore.put("xx-small", 102.0);
        mSizeScore.put("x-small", 103.0);
        mSizeScore.put("small", 104.0);
        mSizeScore.put("medium", 105.0);
        mSizeScore.put("large", 106.0);
        mSizeScore.put("x-large", 107.0);
        mSizeScore.put("2x-large", 108.0);
        mSizeScore.put("3x-large", 109.0);
        mSizeScore.put("4x-large", 110.0);
        mSizeScore.put("5x-large", 111.0);
        mSizeScore.put("6x-large", 112.0);
    }



    public static void sort(List unsortedList, SORT_TYPE type){
        switch (type){
            case SIZE:
                sortSize(unsortedList);
                break;
            case GENERIC:
            default:
                Collections.sort(unsortedList);
        }
    }


    private static void sortSize(List unsortedList){
        Collections.sort(unsortedList, new Comparator<String>(){

            public int compare(String s1, String s2) {
                return getScore(s1).compareTo(getScore(s2));
            }

            public Double getScore(String s1){
                Double score1 = 1000.0;
                s1=s1.replaceAll("\"","");
                if( mSizeScore.containsKey(s1)) {
                    score1 = mSizeScore.get(s1);
                }else{
                    try {
                        score1 = Double.valueOf(s1);
                    }catch (NumberFormatException e){
                        play.Logger.warn(s1 + " can not convert");
                    }
                }
                play.Logger.warn("["+s1+"]["+score1+"]");
                return  score1;
            }
        });
    }

}
