package ai.commerce.xpresso.v1.elasticsearch;

import ai.commerce.xpresso.v1.utils.SortUtils;
import com.mashape.unirest.http.JsonNode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * Converts Elastic Search result to our format and vice versa
 */
public class ElasticSearchResult {

    public static final String ES_RESP_HITS_KEY = "hits";
    public static final String ES_RESP_TOTAL_KEY = "total";
    public static final String ES_RESP_SCORE_KEY = "_score";
    public static final String ES_RESP_SOURCE_KEY = "_source";
    public static final String ES_RESP_AGGS_KEY = "aggregations";
    public static final String ES_RESP_BUCKET_KEY = "buckets";
    public static final String ES_RESP_BUCKET_KEY_KEY = "key";

    public static final String RESPONSE_RESULTS_KEY = "Results";


    /**
     * No of matched results
     */
    private int mFound = 0;

    /**
     * JsonNode instance of the result
     */
    private JsonNode mResult = null;

    public ElasticSearchResult(JsonNode result){
        mResult = result;
    }

    public ElasticSearchResult(){
    }


    public int getNumberOfResults(){
        return mFound;
    }
    /**
     * Convert elastic search query response format to
     * our supported format
     * @return
     */
    public JSONObject convertResultFormat(boolean isProductDetailsTrue){

        if( mResult == null){
            return new JSONObject();
        }

        JSONObject root = mResult.getObject();
        //Check if it contains any hits
        if(!root.has(ES_RESP_HITS_KEY)){
            return new JSONObject();
        }

        JSONObject upperHits = root.getJSONObject(ES_RESP_HITS_KEY);

        if( upperHits.has(ES_RESP_TOTAL_KEY)){
            mFound = upperHits.getInt(ES_RESP_TOTAL_KEY);
        }

        if( mFound == 0){
            // No results
            return new JSONObject();
        }

        JSONArray hitsList = upperHits.getJSONArray(ES_RESP_HITS_KEY);
        JSONObject fullResultObject = new JSONObject();
        Map<String, Set<String>> fieldKeyBasedMap = new HashMap<>();
        for( int i = 0 ; i < hitsList.length(); ++i ){
            JSONObject hit = hitsList.getJSONObject(i);

            if( !hit.has(ES_RESP_SOURCE_KEY)){
                continue;
            }

            JSONObject item = hit.getJSONObject(ES_RESP_SOURCE_KEY);
            if( isProductDetailsTrue ) {

                if(!fullResultObject.has(RESPONSE_RESULTS_KEY)){
                    fullResultObject.put(RESPONSE_RESULTS_KEY, new JSONArray());
                }
                item.put(ES_RESP_SCORE_KEY, hit.getDouble(ES_RESP_SCORE_KEY));
                fullResultObject.getJSONArray(RESPONSE_RESULTS_KEY).put(item);
            }else{
                for( String key: item.keySet()){
                    if( !fieldKeyBasedMap.containsKey(key)){
                        fieldKeyBasedMap.put(key, new HashSet<>());
                    }

                    if( item.get(key) instanceof JSONArray){
                        JSONArray value = item.getJSONArray(key);
                        for( int fi = 0 ; fi < value.length(); ++fi){
                            fieldKeyBasedMap.get(key).add(String.valueOf(value.get(fi)));
                        }

                    }else{
                        fieldKeyBasedMap.get(key).add(String.valueOf(item.get(key)));
                    }
                }
            }
        }

        if( isProductDetailsTrue){
            return fullResultObject;
        }
        JSONObject finalResult = new JSONObject();
        for(Map.Entry<String, Set<String>> entry: fieldKeyBasedMap.entrySet() ){
            List<String> sortedValue = new ArrayList<>();
            sortedValue.addAll(entry.getValue());
            SortUtils.sort(sortedValue, SortUtils.SORT_TYPE.GENERIC);
            finalResult.put(entry.getKey(), sortedValue);
        }
        return finalResult;
    }

    /**
     * Convert elastic search faceted search response format to
     * our supported format
     *
     * "aggregations": {
     "xc_category": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets":[
            "key": "top",
            "doc_count": 16974

     * @return
     */
    public JSONObject convertFacetedResultFormat(){

        if( mResult == null){
            return new JSONObject();
        }

        JSONObject root = mResult.getObject();
        //Check if it contains any hits
        if(!root.has(ES_RESP_AGGS_KEY)){
            return new JSONObject();
        }

        JSONObject aggsKey = root.getJSONObject(ES_RESP_AGGS_KEY);
        Set<String> facetKeys = aggsKey.keySet();

        Map<String, Set<String>> fieldKeyBasedMap = new HashMap<>();
        for(String key: facetKeys){
            JSONObject keyJson = aggsKey.getJSONObject(key);
            if( !keyJson.has(ES_RESP_BUCKET_KEY)){
                continue;
            }
            JSONArray bucket = keyJson.getJSONArray(ES_RESP_BUCKET_KEY);
            for(int i = 0 ; i< bucket.length() ; ++i){
                if( !fieldKeyBasedMap.containsKey(key)){
                    fieldKeyBasedMap.put(key, new HashSet<>());
                }

                JSONObject item = bucket.getJSONObject(i);
                if( !item.has(ES_RESP_BUCKET_KEY_KEY)){
                    continue;
                }
                fieldKeyBasedMap.get(key).add(item.getString(ES_RESP_BUCKET_KEY_KEY));
            }
        }
        JSONObject finalResult = new JSONObject();
        for(Map.Entry<String, Set<String>> entry: fieldKeyBasedMap.entrySet() ){
            List<String> sortedValue = new ArrayList<>();
            sortedValue.addAll(entry.getValue());
            if (entry.getKey().equalsIgnoreCase("size")) {
                SortUtils.sort(sortedValue, SortUtils.SORT_TYPE.SIZE);
            }else{
                SortUtils.sort(sortedValue, SortUtils.SORT_TYPE.GENERIC);
            }
            finalResult.put(entry.getKey(), sortedValue);
        }
        return finalResult;
    }
}