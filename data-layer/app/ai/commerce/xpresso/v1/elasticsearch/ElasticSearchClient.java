package ai.commerce.xpresso.v1.elasticsearch;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import play.Configuration;
import play.Logger;

/**
 *
 * Creates Transport Client connection to
 * elastic search server using IP and PORT.
 *
 * Same client is used to perform queries and retrieve
 * corresponding results.
 */

public class ElasticSearchClient {

    /**
     * HOST Address of elastic search
     */
    private String mHost = "";

    /**
     * PORT of elastic search
     */
    private int mPort = 9200;

    /**
     * Cluster name of elastic search
     */
    private String mClusterName = "";

    /**
     * Cluster Index of elastic search
     * Search happens in this index
     */
    private String mClusterIndex = "";

    /**
     * Cluster Type of elastic search.
     * Used for logical grouping of data
     */
    private String mClusterType = "";

    /**
     * Size of the result in the JSON response
     */
    private int mResultSize = 3000;

    private String mSearchPath = "_search";


    /**
     * Initialize the Elastic Search Client
     * @param configuration configuration file
     */
    public ElasticSearchClient(Configuration configuration){

        mHost = configuration.getString("datalayer.es.cluster_address");
        mPort = configuration.getInt("datalayer.es.cluster_port");
        mClusterName = configuration.getString("datalayer.es.cluster_name");
        mClusterIndex = configuration.getString("datalayer.es.cluster_index");
        mClusterType = configuration.getString("datalayer.es.cluster_type");
    }

    /**
     *
     * Use the Elastic Search REST API to make
     * HTTP Request to the elastic search host and get results.
     * query Builder is used to create json body for HTTP request
     *
     * @param queryBuilder Instance of query which contains query information
     * @return returns the search results
     */
    public ElasticSearchResult performStructuredSearch(ElasticSearchQueryBuilder queryBuilder){
        return performStructuredSearch(queryBuilder, mResultSize);
    }

    /**
     *
     * Use the Elastic Search REST API to make
     * HTTP Request to the elastic search host and get results.
     * query Builder is used to create json body for HTTP request
     *
     * @param queryBuilder Instance of query which contains query information
     * @param resultSize Number of results to fetch
     * @return returns the search results
     */
    public ElasticSearchResult performStructuredSearch(ElasticSearchQueryBuilder queryBuilder, int resultSize){

        ElasticSearchResult response = new ElasticSearchResult();
        queryBuilder.setResultSize(resultSize);

        try {
            Logger.info("["+getSearchURLString()+"]"+queryBuilder.getJSONQuery().toString());

            HttpResponse<JsonNode> jsonResponse = Unirest.post(getSearchURLString())
                    .basicAuth("elastic", "changeme")
                    .header("accept", "application/json")
                    .body(queryBuilder.getJSONQuery().toString())
                    .asJson();

            JsonNode jsonResponseBody = jsonResponse.getBody();
            response = new ElasticSearchResult(jsonResponseBody);
        } catch (UnirestException e) {
            Logger.error("Elastic Search not working:" + e.getMessage());
        }
        return response;
    }

    /**
     * Make HTTP request to elastic search REST API to fetch faceted
     * data
     * @param queryBuilder
     * @return
     */
    public ElasticSearchResult performFacetedSearch(ElasticSearchQueryBuilder queryBuilder){

        ElasticSearchResult response = new ElasticSearchResult();
        queryBuilder.setResultSize(mResultSize);

        try {
            Logger.info(queryBuilder.getFacetedQuery().toString());
            HttpResponse<JsonNode> jsonResponse = Unirest.post(getSearchURLString())
                    .basicAuth("elastic", "changeme")
                    .header("accept", "application/json")
                    .body(queryBuilder.getFacetedQuery().toString())
                    .asJson();

            JsonNode jsonResponseBody = jsonResponse.getBody();
            response = new ElasticSearchResult(jsonResponseBody);
        } catch (UnirestException e) {
            Logger.info("Elastic Search not working:" + e.getMessage());
        }
        return response;
    }

    /**
     * Get elastic search base url string
     */
    public String getSearchURLString(){
        StringBuilder builder = new StringBuilder();
        builder.append("http://");
        builder.append(mHost);
        builder.append(":");
        builder.append(mPort);
        builder.append("/");
        builder.append(mClusterName );
        builder.append("/");
        builder.append(mClusterType);
        builder.append("/");
        builder.append(mSearchPath);

        return builder.toString();
    }

}