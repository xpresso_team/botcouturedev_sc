package ai.commerce.xpresso.v1.elasticsearch;

import ai.commerce.xpresso.v1.data.Constants;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.*;

/**
 *
 * Creates Elastic Search JSON query depending on the provided input
 */

public class ElasticSearchQueryBuilder {

    /**
     * Constant fields which is used to create query json object
     */
    public static final String ES_SOURCE_KEY = "_source";
    public static final String ES_QUERY_KEY = "query";
    public static final String ES_BOOL_KEY = "bool";
    public static final String ES_MUST_KEY = "must";
    public static final String ES_QUERY_STRING_KEY = "query_string";
    public static final String ES_DEFAULT_FIELD_KEY = "default_field";
    public static final String ES_DEFAULT_FIELD_ALL_VALUE_KEY = "_all";
    public static final String ES_MINIMUM_SHOULD_MATCH_KEY = "minimum_should_match";
    public static final String ES_MUST_NOT_KEY = "must_not";
    public static final String ES_SHOULD_KEY = "should";
    public static final String ES_PREFIX_KEY = "prefix";
    public static final String ES_MATCH_KEY = "match";
    public static final String ES_TERM_KEY = "term";
    public static final String ES_FROM_KEY = "from";
    public static final String ES_SIZE_KEY = "size";
    public static final String ES_SORT_KEY = "sort";
    public static final String ES_AGGREGATION_KEY = "aggs";
    public static final String ES_AGGREGATION_TERMS_KEY = "terms";
    public static final String ES_AGGREGATION_FIELD_KEY = "field";
    public static final String ES_RANGE_FIELD_KEY = "range";
    public static final String ES_RANGE_GREATER_THAN_FIELD_KEY = "gte";
    public static final String ES_RANGE_LESS_THAN_FIELD_KEY = "lte";


    /**
     *  Store list of all filters
     */
    HashMap<String, Set<String>> mTermMap = new HashMap<>();

    /**
     *  List of sources
     *  These are fields that is returned from search
     */
    HashMap<String, Set<String>> mPrefixMap = new HashMap<>();

    /**
     *  List of all match phrases which needs to be searched
     */
    HashMap<String, Set<String>> mMatchMap = new HashMap<>();


    /**
     *  List of sources
     *  These are fields that is returned from search
     */
    Set<String> mSources = new HashSet<>();

    /**
     * From and Size of hits in the elastic search query
     */
    private int mResultSize;

    private int mFromIndex = 0;

    /**
     * Faceted field
     * required when we call getFacetedQuery
     */
    private String mFacetedField = "";

    /**
     *  Entity string, this is searched everywhere
     */
    private String mEntity = "";

    /**
     * Set filter strings
     * @param key filter key
     * @param values filter values
     * @return instance
     */
    public ElasticSearchQueryBuilder setTermAttribute(String key, Set<String> values){
        if( mTermMap != null ){
            mTermMap.put(key,values);
        }
        return this;
    }

    /**
     * Set filter strings
     * @param keyValue map of filters
     * @return instance
     */
    public ElasticSearchQueryBuilder setTermAttribute(Map<String, Set<String>> keyValue){
        if ( mTermMap != null){
            mTermMap.putAll(keyValue);
        }
        return this;
    }

    /**
     * Set Entity string.
     * This string is searched everywhere
     */
    public void setEntity(String entity){
        mEntity = entity;
    }

    /**
     * Set return Fields
     * @param fields list of fields which needs to be returned
     * @return instance
     */
    public ElasticSearchQueryBuilder setReturnSourceAttribute(Set<String> fields){
        if ( mSources != null){
            mSources.addAll(fields);
        }
        return this;
    }

    /**
     * Set return Fields
     * @param field field which needs to be returned in response
     * @return instance
     */
    public ElasticSearchQueryBuilder setReturnSourceAttribute(String field){
        if ( mSources != null){
            mSources.add(field);
        }
        return this;
    }

    /**
     * Term will be prefix searched in es
     * @param attr search attribute
     * @return instance
     */
    public ElasticSearchQueryBuilder setPrefixAttribute(String key, Set<String> attr){
        if ( mPrefixMap != null){
            mPrefixMap.put(key, attr);
        }
        return this;
    }

    /**
     * This attributes will be searched as match phrase
     * @param attr search attribute
     * @return instance
     */
    public ElasticSearchQueryBuilder setMatchAttribute(String key, Set<String> attr){
        if ( mMatchMap!= null){
            mMatchMap.put(key, attr);
        }
        return this;
    }

    public void setResultSize(int size){
        mResultSize = size;
    }

    public void setFacetedField(String facetedField){
        mFacetedField = facetedField;
    }

    public void setFromIndex(int fromIndex){
        mFromIndex = fromIndex;
    }

    /**
     * Create json object from the existing parameters according to
     * Elastic Search format.
     * @return
     */
    public JsonNode getJSONQuery(){
        ObjectMapper mapper = new ObjectMapper();

        JsonNode rootNode = mapper.createObjectNode();

        createQueryNode(mapper, (ObjectNode) rootNode);

        //Set Form and Size
        ((ObjectNode) rootNode).put(ES_FROM_KEY, mFromIndex);
        ((ObjectNode) rootNode).put(ES_SIZE_KEY, mResultSize);

        // Set Source Field
        ArrayNode sourceList = ((ObjectNode) rootNode).putArray(ES_SOURCE_KEY);
        mSources.forEach(sourceList::add);

        return rootNode;
    }

    /**
     * Creates complete query node using different attributes provided
     * beforehand
     * @param mapper
     * @param rootNode
     */
    private void createQueryNode(ObjectMapper mapper, ObjectNode rootNode) {
        // Creating query node
        ObjectNode queryNode = rootNode.putObject(ES_QUERY_KEY);
        ObjectNode boolNode = queryNode.putObject(ES_BOOL_KEY);
        ArrayNode shoulcdNode = boolNode.putArray(ES_SHOULD_KEY);
        ArrayNode mustNode = boolNode.putArray(ES_MUST_KEY);

        // Set Entity
        if( !mEntity.isEmpty() ){ 
            ObjectNode queryStringNode = mapper.createObjectNode();
            ObjectNode entityNode = queryStringNode.putObject(ES_QUERY_STRING_KEY);
            entityNode.put(ES_DEFAULT_FIELD_KEY, ES_DEFAULT_FIELD_ALL_VALUE_KEY);
            entityNode.put(ES_QUERY_KEY, mEntity);
            mustNode.add(queryStringNode);
        }

        // Scroll Through Macth Node and insert into should Node
        createBoolNode(mapper, mustNode, mMatchMap, ES_MATCH_KEY);

        // Scroll Through Prefix Node and insert into should Node
        createBoolNode(mapper, mustNode, mPrefixMap, ES_PREFIX_KEY);

        // Scroll Through Term Node and insert into should Node
        createBoolNode(mapper, mustNode, mTermMap, ES_TERM_KEY);
    }

    /**
     * Generic method to crete query Json Object
     * Scroll Through term/should Node and insert into must/should/must_not Node
     *  @param mapper instance of object mapper which is used to create node
     * @param mustNode Parent Node on which current node will be appended
     * @param fieldMap Map of fields which will be inserted
     * @param fieldKey Field Key like prefix/term/wildcard etc.
     */
    private void createBoolNode(ObjectMapper mapper,
                                ArrayNode mustNode,
                                Map<String, Set<String>> fieldMap,
                                String fieldKey) {

        for(Map.Entry<String, Set<String>> entry : fieldMap.entrySet()){
            if(entry.getValue().isEmpty()){
                continue;
            }
            ArrayNode parentNode;
            // If multiple value exist, then apply or value by default
            if(entry.getValue().size() > 1) {
                ObjectNode nestedQuery = mapper.createObjectNode();
                ObjectNode boolNode = nestedQuery.putObject(ES_BOOL_KEY);
                parentNode = boolNode.putArray(ES_SHOULD_KEY);
                boolNode.put(ES_MINIMUM_SHOULD_MATCH_KEY, 1);
                ObjectNode fieldNode = mapper.createObjectNode();
                parentNode.add(fieldNode);
                mustNode.add(nestedQuery);
            }else {
                parentNode = mustNode;
            }

            for (String itemStr : entry.getValue()) {
                if (itemStr.isEmpty()) continue;
                JsonNode fieldNode = mapper.createObjectNode();
                if( entry.getKey().equals(Constants.PRICE_KEY)) {
                    ObjectNode itemNode = ((ObjectNode) fieldNode).putObject(ES_RANGE_FIELD_KEY);
                    ObjectNode priceNode = itemNode.putObject(entry.getKey());

                    if( itemStr.contains(Constants.PRICE_RANGE_KEYWORD)){
                        // <> 90 100
                        itemStr = itemStr.replaceAll(Constants.PRICE_RANGE_KEYWORD, Constants.EMPTY_STRING).trim();
                        String[] priceRange = itemStr.split(Constants.SPACE_STRING);
                        priceNode.put(ES_RANGE_GREATER_THAN_FIELD_KEY, Integer.parseInt(priceRange[0]));
                        priceNode.put(ES_RANGE_LESS_THAN_FIELD_KEY, Integer.parseInt(priceRange[1]));
                    }else if (itemStr.contains(Constants.PRICE_LESS_THAN_KEYWORD)){
                        // < 90
                        itemStr = itemStr.replaceAll(Constants.PRICE_LESS_THAN_KEYWORD, Constants.EMPTY_STRING).trim();
                        priceNode.put(ES_RANGE_LESS_THAN_FIELD_KEY, Integer.parseInt(itemStr));
                    }else if (itemStr.contains(Constants.PRICE_GREATER_THAN_KEYWORD)) {
                        // > 40
                        itemStr = itemStr.replaceAll(Constants.PRICE_GREATER_THAN_KEYWORD, Constants.EMPTY_STRING).trim();
                        priceNode.put(ES_RANGE_GREATER_THAN_FIELD_KEY, Integer.parseInt(itemStr));
                    }

                }else {
                    ObjectNode itemNode = ((ObjectNode) fieldNode).putObject(fieldKey);
                    itemNode.put(entry.getKey(), itemStr);
                }
                parentNode.add(fieldNode);
            }

        }
    }

    /**
     * Generated query to perform faceted search over specific fields
     */
    public JsonNode getFacetedQuery(){
        ObjectMapper mapper = new ObjectMapper();

        JsonNode rootNode = mapper.createObjectNode();

        // Crete query node
        createQueryNode(mapper, (ObjectNode) rootNode);

        // Creating aggs node
        ObjectNode aggsNode = ((ObjectNode) rootNode).putObject(ES_AGGREGATION_KEY);
        ObjectNode keyFieldNode = aggsNode.putObject(mFacetedField);
        ObjectNode termNode = keyFieldNode.putObject(ES_AGGREGATION_TERMS_KEY);
        termNode.put(ES_AGGREGATION_FIELD_KEY, mFacetedField);
        termNode.put(ES_SIZE_KEY, 100);
        //Set Form and Size
        ((ObjectNode) rootNode).put(ES_FROM_KEY, 0);
        ((ObjectNode) rootNode).put(ES_SIZE_KEY, 0);
        return rootNode;
    }
}
