///**
// *
// */
//package ai.commerce.xpresso.v1.domain;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.codehaus.jettison.json.JSONArray;
//import org.codehaus.jettison.json.JSONException;
//import org.codehaus.jettison.json.JSONObject;
//
//import com.abzooba.xcommerce.amazon.dynamodb.AmazonDynamoDB;
//import com.abzooba.xcommerce.config.XCConfig;
//import com.abzooba.xcommerce.core.XCEngine;
//import com.abzooba.xcommerce.core.XCJSON;
//import com.abzooba.xcommerce.nlp.domain.DomainCategoryMapper;
//import com.abzooba.xcommerce.search.SearchItem;
//import com.amazonaws.services.dynamodbv2.document.Item;
//import com.amazonaws.services.dynamodbv2.document.Table;
//import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
//import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
//import com.amazonaws.services.dynamodbv2.model.AttributeValue;
//import com.amazonaws.services.dynamodbv2.model.ReturnValue;
//import com.amazonaws.services.dynamodbv2.model.ScanRequest;
//import com.amazonaws.services.dynamodbv2.model.ScanResult;
//
//import edu.emory.mathcs.backport.java.util.Collections;
//
///**
// * @author sudhanshu.kumar@abzooba.com
// *
// */
//public class AmazonDynamoDBXCFashion extends AmazonDynamoDB {
//
//
//	/*
//	 * return list of clients in the db.
//	 * no input is requierd query can be null.
//	 * */
//	public static JSONObject retrieveClientList(Map<String, String[]> query) {
//		//		String domainName = query.get("src")[0];
//		String domainName = XCConfig.DEFAULT_DATA_DOMAIN;
//		JSONObject ja = new JSONObject();
//		try {
//			Set<String> check = new HashSet<String>();
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			scanRequest.withProjectionExpression("client_name, client_id");
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				if (!check.contains(item.get("client_id").getS())) {
//					check.add((item.get("client_id").getS()));
//					ja.put(item.get("client_id").getS(), item.get("client_name").getS());
//				}
//			}
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//		}
//		return ja;
//	}
//
//	public static JSONObject retrieveXCCategory(Map<String, String[]> query) {
//		//		String domainName = query.get("src")[0];
//		String domainName = XCConfig.DEFAULT_DATA_DOMAIN;
//		String gender = query.get("gender")[0];
//		JSONObject joReturn = new JSONObject();
//		JSONArray jaXCCategory;
//		try {
//			Set<String> check = new HashSet<String>();
//			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//			expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
//
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			scanRequest.withFilterExpression("gender = :gender ");
//			scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//			scanRequest.withProjectionExpression("xc_category");
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				for (String xcCat : item.get("xc_category").getSS()) {
//					if (!check.contains(xcCat)) {
//						check.add((xcCat));
//						//						jaXCCategory.put(xcCat);
//					}
//				}
//			}
//			check.remove("others");
//			List<String> list = new ArrayList<String>(check);
//			Collections.sort(list);
//			jaXCCategory = new JSONArray(list);
//			joReturn.put("gender", gender);
//			joReturn.put("xc_category", jaXCCategory);
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//		}
//		System.out.println(joReturn.toString());
//		return joReturn;
//	}
//
//	public static JSONObject retrieveSizeList(Map<String, String[]> query) {
//		String domainName = XCConfig.DEFAULT_DATA_DOMAIN;
//		String gender = query.get("gender")[0];
//		String xc_category = query.get("xc_category")[0];
//		logger.info("xc_category : " + xc_category + ". Gender : " + gender);
//		JSONObject joReturn = new JSONObject();
//		JSONArray jArr;
//		try {
//			Set<String> check = new HashSet<String>();
//			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//			expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
//			expressionAttributeValues.put(":xc_category", new AttributeValue().withSS(xc_category));
//
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			scanRequest.withFilterExpression("xc_category = :xc_category AND gender = :gender ");
//			scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//			scanRequest.withProjectionExpression("size");
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				if (item.get("size") != null) {
//					for (String subCat : item.get("size").getSS()) {
//						//					System.out.print(subCat + "\t");
//						if (!check.contains(subCat)) {
//							check.add(subCat);
//							//						jArr.put(subCat);
//						}
//					}
//				}
//			}
//
//			List<String> list = new ArrayList<String>(check);
//			Collections.sort(list);
//			jArr = new JSONArray(list);
//			joReturn.put("gender", gender);
//			joReturn.put("xc_category", xc_category);
//			joReturn.put("size", jArr);
//			logger.info(joReturn.toString());
//		} catch (Exception e) {
//			logger.info(e.toString());
//			logger.error(e.getMessage());
//			e.printStackTrace();
//		}
//		return joReturn;
//	}
//
//	/**
//	 *
//	 * @param query map should have src, client and gender
//	 * @return - List of subcategories with given gender and client
//	 */
//	public static JSONObject retrieveClientSubCategory(Map<String, String[]> query) {
//		//		String S3_URL = DomainCategoryMapper.getS3Url(domainName);
//		//		String domainName = query.get("src")[0];
//		String domainName = XCConfig.DEFAULT_DATA_DOMAIN;
//		String client_name = query.get("client")[0];
//		String gender = query.get("gender")[0];
//		JSONObject netJSONObject = new JSONObject();
//		try {
//			JSONArray ja = new JSONArray();
//			Set<String> check = new HashSet<String>();
//			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//			expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
//			expressionAttributeValues.put(":client_name", new AttributeValue().withS(client_name));
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			scanRequest.withFilterExpression("gender = :gender AND client_name = :client_name ");
//			scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//			scanRequest.withProjectionExpression("subcategory");
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//			int totalCount = 0;
//
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				logger.info(item.get("subcategory").toString());
//				if (!check.contains(item.get("subcategory"))) {
//					check.add(item.get("subcategory").toString());
//					//					JSONObject mainObj = new JSONObject();
//					//					mainObj.put("subcategory", item.get("subcategory").toString());
//					//					mainObj.put("image", S3_URL + item.get("sku").getS());
//					ja.put(item.get("subcategory").toString());
//					++totalCount;
//				}
//			}
//			netJSONObject.put(XCJSON.RESULTS_NODE, ja);
//			netJSONObject.put(XCJSON.RESULTS_COUNT_NODE, totalCount);
//		} catch (Exception e) {
//			logger.info(e.toString());
//			logger.error(e.getMessage());
//		}
//		return netJSONObject;
//	}
//
//	/*
//	 * gives list of subcategory for given xpresso commerce standard category.
//	 * Client name is optional to get client specific subcategory.
//	 * */
//	public JSONArray getClientSubCategory(String xc_category, String client_name) {
//		logger.info("xc_category : " + xc_category + ". Client : " + client_name);
//		String domainName = XCConfig.DEFAULT_DATA_DOMAIN;
//		JSONArray jArr = new JSONArray();
//		try {
//			Set<String> check = new HashSet<String>();
//			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//			expressionAttributeValues.put(":xc_category", new AttributeValue().withSS(xc_category));
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//
//			if (client_name.isEmpty() || client_name.equalsIgnoreCase("all")) {
//				scanRequest.withFilterExpression("xc_category = :xc_category");
//			} else {
//				expressionAttributeValues.put(":client_name", new AttributeValue().withS(client_name));
//				scanRequest.withFilterExpression("xc_category = :xc_category AND client_name = :client_name ");
//			}
//
//			scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//			scanRequest.withProjectionExpression("subcategory");
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				for (String subCat : item.get("subcategory").getSS()) {
//					//					System.out.print(subCat + "\t");
//					if (!check.contains(subCat)) {
//						check.add(subCat);
//						jArr.put(subCat);
//					}
//				}
//			}
//
//			logger.info(jArr.toString());
//		} catch (Exception e) {
//			logger.info(e.toString());
//			logger.error(e.getMessage());
//		}
//		return jArr;
//	}
//
//	/**
//	 *
//	 * @param query map should have src, client and gender and subcategory
//	 * @return - List of items for given subcategory
//	 */
//	public static JSONObject retrieveClientItems(Map<String, String[]> query) {
//		//		String S3_URL = DomainCategoryMapper.getS3Url(domainName);
//		String domainName = query.get("src")[0];
//		String client_name = query.get("client")[0];
//		String gender = query.get("gender")[0];
//		String subcategory = query.get("subcategory")[0];
//		JSONObject netJSONObject = new JSONObject();
//		try {
//			JSONArray ja = new JSONArray();
//			Set<String> check = new HashSet<String>();
//			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//			expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
//			expressionAttributeValues.put(":client_name", new AttributeValue().withS(client_name));
//			expressionAttributeValues.put(":subcategory", new AttributeValue().withSS(subcategory));
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			scanRequest.withFilterExpression("gender = :gender AND client_name = :client_name AND subcategory = :subcategory ");
//			scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//			scanRequest.withProjectionExpression("sku, image, price, productname, colorsavailable");
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//			int totalCount = 0;
//			logger.info(result.getCount().toString());
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				logger.info(item.get("sku").toString());
//				if (!check.contains(item.get("subcategory"))) {
//					check.add(item.get("subcategory").toString());
//					JSONObject mainObj = new JSONObject();
//					mainObj.put("sku", item.get("sku").toString());
//					//					mainObj.put("image", S3_URL + item.get("sku").getS());
//					ja.put(mainObj);
//					++totalCount;
//				}
//			}
//			netJSONObject.put(XCJSON.RESULTS_NODE, ja);
//			netJSONObject.put(XCJSON.RESULTS_COUNT_NODE, totalCount);
//		} catch (Exception e) {
//			logger.info(e.toString());
//			logger.error(e.getMessage());
//		}
//		return netJSONObject;
//	}
//
//	/**
//	 *
//	 * @param domainName
//	 * @param gender
//	 * @return - List of items with given gender
//	 */
//	public static JSONObject retrieveAll(String domainName, String gender) {
//		//		String S3_URL = DomainCategoryMapper.getS3Url(domainName);
//		JSONObject netJSONObject = new JSONObject();
//		try {
//			JSONArray ja = new JSONArray();
//			//			Set<String> check = new HashSet<String>();
//			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//			expressionAttributeValues.put(":gender", new AttributeValue().withS(gender.toLowerCase().trim()));
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			scanRequest.withFilterExpression("gender = :gender");
//			scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//			scanRequest.withProjectionExpression("category,image,sku");
//			//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//			int totalCount = 0;
//			//			for (Map<String, AttributeValue> item : result.getItems()) {
//			//				if (!check.contains(item.get("category").getSS())) {
//			//					check.addAll((item.get("category").getSS()));
//			//					JSONObject mainObj = new JSONObject();
//			//					mainObj.put("category", item.get("category").getSS());
//			//					mainObj.put("image", S3_URL + item.get("sku").getS());
//			//					ja.put(mainObj);
//			//					++totalCount;
//			//				}
//			//			}
//			netJSONObject.put(XCJSON.RESULTS_NODE, ja);
//			netJSONObject.put(XCJSON.RESULTS_COUNT_NODE, totalCount);
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//		}
//		return netJSONObject;
//	}
//
//	/**
//	 *
//	 * @param domainName
//	 * @param subcategory
//	 * @return - List of sub categories
//	 */
//	//	public static List<?> retrieveAllSubCategory(String domainName, String subcategory) {
//	//		logger.info("Called retrieveAll with " + domainName + "\t" + subcategory);
//	//		subcategory = ProcessString.toCamelCase(subcategory);
//	//		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//	//		expressionAttributeValues.put(":subcategory", new AttributeValue().withS(subcategory));
//	//		ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//	//		scanRequest.withFilterExpression("subcategory = :subcategory");
//	//		scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//	//		scanRequest.withProjectionExpression("productname,brand,price,image,sku,subcategory");
//	//		ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//	//		return result.getItems();
//	//	}
//
//	/**
//	 *
//	 * @param domainName
//	 * @param gender
//	 * @param category
//	 * @return - List of items with given gender and category
//	 */
//	public static JSONObject retrieveAll(String domainName, String gender, String category) {
//		String S3_URL = DomainCategoryMapper.getS3Url(domainName);
//		JSONObject netJSONObject = new JSONObject();
//		try {
//			JSONArray ja = new JSONArray();
//			Set<String> check = new HashSet<String>();
//			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//			expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
//			expressionAttributeValues.put(":category", new AttributeValue().withS(category));
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			scanRequest.withFilterExpression("gender = :gender AND category = :category ");
//			scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//			scanRequest.withProjectionExpression("subcategory,image,sku");
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//			int totalCount = 0;
//
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				if (!check.contains(item.get("subcategory").getS())) {
//					check.add(item.get("subcategory").getS());
//					JSONObject mainObj = new JSONObject();
//					mainObj.put("subcategory", item.get("subcategory").getS());
//					mainObj.put("image", S3_URL + item.get("sku").getS());
//					ja.put(mainObj);
//					++totalCount;
//				}
//			}
//			netJSONObject.put(XCJSON.RESULTS_NODE, ja);
//			netJSONObject.put(XCJSON.RESULTS_COUNT_NODE, totalCount);
//		} catch (Exception e) {
//			logger.info(e.toString());
//			logger.error(e.getMessage());
//		}
//		return netJSONObject;
//	}
//
//	/**
//	 *
//	 * @param domainName
//	 * @param gender
//	 * @param category
//	 * @param subcategory
//	 * @return - Items with given filters
//	 */
//	public static List<?> retrieveAll(String domainName, String gender, String category, String subcategory) {
//		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//		expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
//		expressionAttributeValues.put(":category", new AttributeValue().withS(category));
//		expressionAttributeValues.put(":subcategory", new AttributeValue().withS(subcategory));
//		ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//		scanRequest.withFilterExpression("gender = :gender AND category = :category AND subcategory = :subcategory");
//		scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//		scanRequest.withProjectionExpression("productname,brand,price,image,sku");
//		ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//		return result.getItems();
//	}
//
//	public static JSONObject retrieveAll(String domainName, String gender, String category, String subcategory, Boolean isSuggested, int page, int perPage) {
//		List<?> resultItems = retrieveAll(domainName, gender, category, subcategory);
//		SearchItem objSearchItem = new SearchItem();
//		List<SearchItem> searchItemList = objSearchItem.getSearchItemList(resultItems, isSuggested);
//		return XCJSON.processResultsToJSON("", "", "", domainName, searchItemList, page, perPage, null, null);
//	}
//
//	/**
//	 *
//	 * @param domainName
//	 * @param gender
//	 * @param category
//	 * @param subcategory
//	 * @param section
//	 * @return - Items with given filters
//	 */
//	public static List<?> retrieveAll(String domainName, String gender, String category, String subcategory, String section) {
//		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//		Map<String, String> expressionAttributeNames = new HashMap<String, String>();
//		expressionAttributeValues.put(":gender", new AttributeValue().withS(gender));
//		expressionAttributeValues.put(":category", new AttributeValue().withS(category));
//		expressionAttributeValues.put(":subcategory", new AttributeValue().withS(subcategory));
//		expressionAttributeValues.put(":subsubcategory", new AttributeValue().withS(section));
//		expressionAttributeNames.put("#s", "Section");
//		ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//		scanRequest.withFilterExpression("gender = :gender AND category = :category AND subcategory = :subcategory AND #s = :subsubcategory");
//		scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//		scanRequest.withExpressionAttributeNames(expressionAttributeNames);
//		scanRequest.withProjectionExpression("productname,brand,price,image,sku");
//		ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//		List<?> resultItems = result.getItems();
//		return resultItems;
//	}
//
//	/**
//	 *
//	 * @param domainName - customer name
//	 * @return - Items with given attributes in batch
//	 */
//	public static JSONArray retrieveInBatches(String domainName) {
//		Map<String, AttributeValue> lastEvaluatedKey = null;
//		logger.info("retrieve module entered");
//		int counter = 0;
//		try {
//			JSONArray ja = new JSONArray();
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			if (lastEvaluatedKey != null)
//				scanRequest.setExclusiveStartKey(lastEvaluatedKey);
//			//			if ( > 0)
//			//				scanRequest.setLimit(AWSConfig.maxReviewCount);
//			//			else
//			//				scanRequest.setLimit(500);
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//			if (result == null)
//				return ja;
//			lastEvaluatedKey = result.getLastEvaluatedKey();
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				if (counter >= XCConfig.maxReviewCount)
//					break;
//				logger.info("counter - " + counter++);
//				JSONObject mainObj = new JSONObject();
//				mainObj.put("category", item.get("category").getS());
//				mainObj.put("image", item.get("image").getS());
//				mainObj.put("sku", item.get("sku").getS());
//				mainObj.put("gender", item.get("gender").getS());
//				mainObj.put("price", item.get("price").getS());
//				mainObj.put("subcategory", item.get("subcategory").getS());
//				mainObj.put("brand", item.get("brand").getS());
//				mainObj.put("sizesavailable", item.get("sizesavailable"));
//				mainObj.put("colorsavailable", item.get("colorsavailable"));
//				mainObj.put("productname", item.get("productname"));
//				ja.put(mainObj);
//			}
//			logger.info("Items retrieved");
//			return ja;
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			return null;
//		}
//
//	}
//
//	/**
//	 * Returns Item details based on unique key SKU and domain name
//	 * @param domainName - customer name
//	 * @param sku - unique key
//	 * @return - Json string about details of the item
//	 */
//	public static String retrieveItem(String domainName, String sku) {
//		Table table = dynamoDB.getTable(DomainCategoryMapper.getTableName(domainName));
//		try {
//			Item item = table.getItem("xc_sku", sku);
//			if (item != null) {
//				//				item.withString("image", DomainCategoryMapper.getS3Url(domainName) + sku.toUpperCase().trim());
//				return item.toJSONPretty();
//			} else {
//				return "";
//			}
//		} catch (Exception e) {
//			logger.error("retrieveItem failed.");
//			e.printStackTrace();
//			logger.error(e.getMessage());
//			return null;
//		}
//	}
//
//	public static void updateImageLinks(String domainName) {
//		String S3_URL = DomainCategoryMapper.getS3Url(domainName);
//		try {
//			ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//			ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//			Table table = dynamoDB.getTable(DomainCategoryMapper.getTableName(domainName));
//			int cnt = 0;
//			String sku, url;
//			UpdateItemSpec updateItemSpec = null;
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				cnt++;
//				sku = item.get("sku").getS();
//				url = S3_URL + sku;
//				try {
//					updateItemSpec = new UpdateItemSpec().withPrimaryKey("sku", sku).withUpdateExpression("set image = :sku").withValueMap(new ValueMap().withString(":sku", url)).withReturnValues(ReturnValue.NONE);
//					table.updateItem(updateItemSpec);
//				} catch (Exception e) {
//					logger.error("Unable to update item: " + sku);
//					logger.error(e.getMessage());
//				}
//				logger.info(cnt + "");
//			}
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//		}
//	}
//
//	/**
//	 *
//	 * @param domainName
//	 * @param gender
//	 * @param category
//	 * @param returnProduct
//	 * @return - Items with given attributes in form of List<Map<String, AttributeValue>>
//	 */
//	//	public static List<?> retrieveAll(String domainName, String gender, String category, Boolean returnProduct) {
//	//		logger.info("Called retrieveAll with " + domainName + "\t" + gender + "\t" + category);
//	//		category = ProcessString.toCamelCase(category);
//	//		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
//	//		expressionAttributeValues.put(":gender", new AttributeValue().withS(gender.toLowerCase()));
//	//		expressionAttributeValues.put(":category", new AttributeValue().withSS(category.toLowerCase()));
//	//		ScanRequest scanRequest = new ScanRequest().withTableName(DomainCategoryMapper.getTableName(domainName));
//	//		scanRequest.withFilterExpression("gender = :gender AND category = :category");
//	//		scanRequest.withExpressionAttributeValues(expressionAttributeValues);
//	//		scanRequest.withProjectionExpression("productname,brand,price,image,sku,subcategory");
//	//		ScanResult result = amazonDynamoDBClient.scan(scanRequest);
//	//		return result.getItems();
//	//	}
//
//	//	public static JSONObject retrieveAll(String domainName, String gender, String category, Boolean returnProduct, Boolean isSuggested, int page, int perPage) {
//	//		logger.info("Called retrieveAll with " + domainName + "\t" + gender + "\t" + category);
//	//		List<?> resultItems = retrieveAll(domainName, gender, category, returnProduct);
//	//		SearchItem objSearchItem = new SearchItem();
//	//		List<SearchItem> searchItemList = objSearchItem.getSearchItemList(resultItems, isSuggested);
//	//		return XCJSON.processResultsToJSON("", "", "", domainName, searchItemList, page, perPage, null, null);
//	//	}
//
//	//	public static JSONObject retrieveAll(HashMap<String, String[]> params) {
//	//		if (params.containsKey("returnProduct"))
//	//			return retrieveAll(params.get("src")[0], params.get("gender")[0], params.get("category")[0], false, false, Integer.parseInt(params.get("page")[0]), Integer.parseInt(params.get("perPage")[0]));
//	//		else
//	//			return retrieveAll(params.get("src")[0], params.get("gender")[0], params.get("category")[0], params.get("subcategory")[0], false, Integer.parseInt(params.get("page")[0]), Integer.parseInt(params.get("perPage")[0]));
//	//	}
//
//	public static void main(String[] args) {
//		XCEngine.init();
//		Map<String, String[]> queryParameters = new HashMap<String, String[]>();
//		//		queryParameters.put("src", new String[] { "debenhams" });
//		queryParameters.put("gender", new String[] { "women" });
//		queryParameters.put("xc_category", new String[] { "leggings" });
//		//		queryParameters.put("client", new String[] { "macys" });
//		//		queryParameters.put("subcategory", new String[] { "S: shirt" });
//		//		JSONObject ja = new JSONObject();
//		//		ja = retrieveClientList(queryParameters);
//		//		ja = retrieveClientSubCategory(queryParameters);
//		//		ja = retrieveClientItems(queryParameters);
//		//		getClientSubCategory("shirt", "");
//		//		retrieveXCCategory(queryParameters);
//		retrieveSizeList(queryParameters);
//		//		System.out.println(ja.toString());
//	}
//}
