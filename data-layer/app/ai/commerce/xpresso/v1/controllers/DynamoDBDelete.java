package ai.commerce.xpresso.v1.controllers;

import ai.commerce.xpresso.v1.amazon.dynamodb.AmazonDynamoDB;
import com.google.inject.Inject;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.Map;
import java.util.Set;

/**
 * Created by naveen on 1/5/17.
 *
 * Handles all post request for the AWS Dynamodb.
 * Uses AWS SDK to perform put item request to the database.
 *
 * It supports batch request to the server. By default always batch update is performed.
 *
 * It has following api:
 *  1) put list of products in dynamodb.
 */

public class DynamoDBDelete extends Controller {
    @Inject
    private Configuration mConfiguration;

    // Instance of amazon dynamo db
    private AmazonDynamoDB mAmazonDynamoDB = null;

    @Inject
    public DynamoDBDelete(Configuration configuration){
        mAmazonDynamoDB = new AmazonDynamoDB(configuration);
    }


    /**
     * Fetch single product using the primary key eg. xc_sku
     * @return Returns the complete detail of the products
     */
    public Result deleteProduct(){
        final Set<Map.Entry<String,String[]>> entries = request().queryString().entrySet();
        String[] keyValue = new String[0];
        // Default key name is xc_sku
        String keyName = mConfiguration.getString("datalayer.dynamodb.default_key_name");
        String tableName = mConfiguration.getString("datalayer.dynamodb.default_table_name");

        for (Map.Entry<String,String[]> entry : entries) {
            if ( entry.getKey().equalsIgnoreCase("keyValue") &&
                    entry.getValue().length > 0){
                keyValue = entry.getValue();
            }else if(entry.getKey().equalsIgnoreCase("keyName") &&
                    entry.getValue().length > 0){
                keyName = entry.getValue()[0];
            }
            else if ( entry.getKey().equalsIgnoreCase("tableName") &&
                    entry.getValue().length > 0) {
                tableName = entry.getValue()[0];
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }

        JSONObject finalResults = new JSONObject();
        if( keyValue.length > 0 ){

            finalResults = mAmazonDynamoDB.deleteProduct(tableName, keyName, keyValue);

        }else{
            try {
                finalResults.put("status", "fail");
                finalResults.put("code", 200);
                finalResults.put("msg", "no key provided");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return ok(finalResults.toString()).as("application/json");
    }
}
