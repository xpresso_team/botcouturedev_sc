package ai.commerce.xpresso.v1.controllers;

import ai.commerce.xpresso.v1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1.elasticsearch.ElasticSearchClient;
import ai.commerce.xpresso.v1.elasticsearch.ElasticSearchQueryBuilder;
import ai.commerce.xpresso.v1.elasticsearch.ElasticSearchResult;
import ai.commerce.xpresso.v1.search.SearchItem;
import ai.commerce.xpresso.v1.size.SizeChart;
import ai.commerce.xpresso.v1.utils.XCJSON;
import com.google.inject.Inject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.*;

/**
 * Created by naveen on 1/5/17.
 *
 * Handles all GET request for the Elastic Search.
 * Uses AWS SDK to create a structured query and perform search
 *
 * It has following api:
 *  1) Generic API -- Performs search on the basis of filter parameters given.
 *
 *  2) StructuredSearch -- Similar to generic API
 */

public class ElasticSearchGet extends Controller {
    @Inject
    private Configuration mConfiguration;

    private ElasticSearchClient mElasticSearch;

    @Inject
    public ElasticSearchGet(Configuration configuration){

        mElasticSearch = new ElasticSearchClient(configuration);
    }

    /**
     * Performs search on multiple fields in cloud search and
     * return field can contain any fields or complete product details
     *
     * @return Results
     */

    public Result genericAPI() {

        // Few search configurations.
        String[] returnField = {
                mConfiguration.getString("datalayer.es.default_return_field")
        };
        boolean productDetails = false;

        // Map to store filtering parameter.
        // These parameters are used for filtering in cloudsearch request
        Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>();

        // Iterating over provided parameters.
        // Identifies the requirement of request.
        // Fetch all results into a map
        final Set<Map.Entry<String, String[]>> entries = request().queryString().entrySet();
        String cursor = "initial";
        int pageSize = mConfiguration.getInt("datalayer.cloudsearch.max_search_hits");
        boolean hasSize = false;
        Set<String> originalSize = new HashSet<>();

        for (Map.Entry<String, String[]> entry : entries) {
            if (entry.getKey().equalsIgnoreCase("returnField") &&
                    entry.getValue().length > 0) {

                // Check if comma separated values are given in single key
                if( entry.getValue().length == 1){
                    returnField =  entry.getValue()[0].split(",");
                }else {
                    returnField = entry.getValue();
                }
            }else if(entry.getKey().equalsIgnoreCase("cursor") &&
                    entry.getValue().length > 0){

                cursor = entry.getValue()[0];
            }else if(entry.getKey().equalsIgnoreCase("pageSize") &&
                    entry.getValue().length > 0){

                try{
                    pageSize = Integer.parseInt(entry.getValue()[0]);
                }catch (NumberFormatException e){
                    Logger.warn(e.getMessage());
                }
            } else if (entry.getKey().equalsIgnoreCase("productDetails") &&
                    entry.getValue().length > 0) {
                productDetails = Boolean.parseBoolean(entry.getValue()[0]);

            } else if(entry.getKey().equalsIgnoreCase("size") &&
                entry.getValue().length > 0) {
                //Convert size here
                Set<String> setValues = new HashSet<String>(Arrays.asList(entry.getValue()));
                originalSize = setValues;
                hasSize = true;
            }
            else {
                Set<String> setValues = new HashSet<>(Arrays.asList(entry.getValue()));
                if( entry.getValue().length == 1){
                    setValues = new HashSet<>(Arrays.asList(entry.getValue()[0].split(",")));
                }
                filterMap.put(entry.getKey(), setValues);
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }

        if( XCConfig.ENABLE_STANDARD_SIZE &&
                hasSize &&
                (filterMap.containsKey("xc_category") || filterMap.containsKey("xc_hierarchy_str"))) {
            //get xc_category
            String sizeCategory = "apparel";
            if( filterMap.containsKey("xc_category") ){
                // Pick first category
                Set<String> category = filterMap.get("xc_category");
                Iterator<String> it = category.iterator();
                if( it.hasNext()){
                    sizeCategory = it.next();
                }
                if( sizeCategory.equalsIgnoreCase("footwear")){
                    sizeCategory = "footwear";
                }
            } else if( filterMap.containsKey("xc_hierarchy_str") ){
                // Pick first category
                Set<String> category = filterMap.get("xc_hierarchy_str");
                Iterator<String> it = category.iterator();
                if( it.hasNext()){
                    sizeCategory = it.next();
                }
                if( sizeCategory.startsWith("footwear")){
                    sizeCategory = "footwear";
                }
            }
            Logger.info("Size Category-"+sizeCategory);
            Set<String> standardValues = getStandardSize(originalSize,sizeCategory);
            filterMap.put("size", standardValues);
        }


        boolean isFaceted = false;
        if( returnField.length == 1 &&
                (returnField[0].equalsIgnoreCase("xc_category") ||
                        returnField[0].equalsIgnoreCase("client_id") ||
                        returnField[0].equalsIgnoreCase("client_name") ||
                        returnField[0].equalsIgnoreCase("size")) ){
            isFaceted= true;
            pageSize = 1;
            cursor = "initial";
        }

        JSONObject finalResult = new JSONObject();
        try {
            for (Map.Entry<String, Set<String>> entry : filterMap.entrySet()) {
                finalResult.put(entry.getKey(), entry.getValue());
            }
            // Insert original  size
            if( hasSize ){
                finalResult.put("size",originalSize);
            }

            ElasticSearchQueryBuilder queryBuilder = new ElasticSearchQueryBuilder();
            queryBuilder.setTermAttribute(filterMap);
            queryBuilder.setReturnSourceAttribute(new HashSet<String>(Arrays.asList(returnField)));


            JSONObject returnValues = null;

            ElasticSearchResult searchResult = null;
            if( isFaceted ){
                queryBuilder.setFacetedField(returnField[0]);
                searchResult = mElasticSearch.performFacetedSearch(queryBuilder);
                returnValues = searchResult.convertFacetedResultFormat();

            }else{
                searchResult = mElasticSearch.performStructuredSearch(queryBuilder, pageSize);
                returnValues = searchResult.convertResultFormat(productDetails);
            }
            Logger.info("Result converted");
            finalResult.put("Results", returnValues);
            finalResult.put("resultsCount", searchResult.getNumberOfResults());
            finalResult.put("found", searchResult.getNumberOfResults());
            finalResult.put("cursor", "Not yet implemented");
            finalResult.put("code",200);
            finalResult.put("status","ok");
            finalResult.put("msg","ok");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ok(finalResult.toString()).as("application/json");
    }


    /**
     * Call standard size
     * @param sizes
     * @return
     */
    private Set<String> getStandardSize( Set<String> sizes, String xcCategory){
        Set<String> standardValues = new HashSet<>();
        for( String size: sizes) {
            standardValues.add(SizeChart.getInstance().convertSize(size,xcCategory));
        }
        return standardValues;
    }


    /**
     * Perform structured search over cloudsearch
     * @return Results
     */

    public Result structuredSearchAPI() {

        String entityStr = "";

        final Set<Map.Entry<String, String[]>> entries = request().queryString().entrySet();
        // Map to store filtering parameter.
        // These parameters are used for filtering in cloudsearch request
        Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>();
        List<String> prefixList = new ArrayList<>();

        boolean hasSize = false;
        Set<String> originalSize = new HashSet<>();

        Integer resultSize = mConfiguration.getInt("datalayer.es.result_size");


        for (Map.Entry<String, String[]> entry : entries) {
            if(entry.getKey().equalsIgnoreCase("entity") &&
                    entry.getValue().length > 0){
                entityStr = entry.getValue()[0];
            }else if(entry.getKey().equalsIgnoreCase("prefixFields") &&
                    entry.getValue().length > 0){
                if( 1 == entry.getValue().length ){
                    prefixList = Arrays.asList(entry.getValue()[0].split(","));
                }else {
                    prefixList = Arrays.asList(entry.getValue());
                }
            }else if(entry.getKey().equalsIgnoreCase("pageSize") &&
                    entry.getValue().length > 0){

                try{
                    resultSize = Integer.parseInt(entry.getValue()[0]);
                }catch (NumberFormatException e){
                    Logger.warn(e.getMessage());
                }
            }else if(entry.getKey().equalsIgnoreCase("size") &&
                     entry.getValue().length > 0) {

                //Convert size here
                Set<String> setValues = new HashSet<String>(Arrays.asList(entry.getValue()));
                originalSize = setValues;
                hasSize = true;
            }else{
                Set<String> setValues;
                if( 1 == entry.getValue().length ) {
                    setValues = new HashSet<String>(Arrays.asList(entry.getValue()[0].split(",")));
                }else {
                    setValues = new HashSet<String>(Arrays.asList(entry.getValue()));
                }
                filterMap.put(entry.getKey(), setValues);
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }

        if( filterMap.containsKey("xc_hierarchy_str") &&
                !prefixList.contains("xc_hierarchy_str")){
            prefixList.add("xc_hierarchy_str");
        }

        // Set category
        if( XCConfig.ENABLE_STANDARD_SIZE &&
                hasSize &&
                (filterMap.containsKey("xc_category") || filterMap.containsKey("xc_hierarchy_str"))) {
            //get xc_category
            String sizeCategory = "apparel";
            if( filterMap.containsKey("xc_category") ){
                // Pick first category
                Set<String> category = filterMap.get("xc_category");
                Iterator<String> it = category.iterator();
                if( it.hasNext()){
                    sizeCategory = it.next();
                }
                if( sizeCategory.equalsIgnoreCase("footwear")){
                    sizeCategory = "footwear";
                }
            }else if( filterMap.containsKey("xc_hierarchy_str") ){
                // Pick first category
                Set<String> category = filterMap.get("xc_hierarchy_str");
                Iterator<String> it = category.iterator();
                if( it.hasNext()){
                    sizeCategory = it.next();
                }
                if( sizeCategory.startsWith("footwear")){
                    sizeCategory = "footwear";
                }
            }
            Logger.info("Size Category-"+sizeCategory);
            Set<String> standardValues = getStandardSize(originalSize,sizeCategory);
            filterMap.put("size", standardValues);
        }

        /*
         * Pick fields to return from database
         */
        List<String> returnFieldList = mConfiguration
                .getStringList("datalayer.cloudsearch.structured_search_return_fields");
        String[] returnField = returnFieldList.toArray(new String[returnFieldList.size()]);

        // Build Query using provided parameters
        ElasticSearchQueryBuilder queryBuilder = new ElasticSearchQueryBuilder();
        queryBuilder.setEntity(entityStr);
        queryBuilder.setResultSize(resultSize);
        for( Map.Entry<String, Set<String>> entry: filterMap.entrySet()){
            if( entry.getKey().equalsIgnoreCase("src")){
                if(!entry.getValue().contains("all") ) {
                    queryBuilder.setMatchAttribute("client_name", entry.getValue());
                }
            }else if(entry.getKey().equalsIgnoreCase("brand")){
                    queryBuilder.setMatchAttribute(entry.getKey(), entry.getValue());
            } else{
                queryBuilder.setMatchAttribute(entry.getKey(), entry.getValue());
            }
        }
        queryBuilder.setReturnSourceAttribute(new HashSet<>(Arrays.asList(returnField)));
        for(String prefix : prefixList) {
            queryBuilder.setPrefixAttribute(prefix, filterMap.get(prefix));
        }

        JSONObject finalResult = new JSONObject();
        try {
            for (Map.Entry<String, Set<String>> entry : filterMap.entrySet()) {
                Set<String> values = entry.getValue();
                if( prefixList.contains(entry.getKey()) ){
                    Set<String> tempValue = new HashSet<>();
                    for( String value : values){
                        tempValue.add(value+"*");
                    }
                    values = tempValue;
                }
                finalResult.put(entry.getKey(), values);
            }
            // Insert original  size
            if( hasSize ){
                finalResult.put("size",originalSize);
            }

            ElasticSearchResult searchResult = mElasticSearch.performStructuredSearch(
                    queryBuilder, resultSize);

            JSONObject returnValues = searchResult.convertResultFormat(true);

            finalResult.put("entity",entityStr);
            if( returnValues != null && returnValues.has("Results")) {
                finalResult.put("Results", returnValues.getJSONArray("Results"));
            }else{
                finalResult.put("Results", new JSONArray());
            }
            finalResult.put("resultsCount", searchResult.getNumberOfResults());
            finalResult.put("code",200);
            finalResult.put("status","ok");
            finalResult.put("msg","ok");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ok(finalResult.toString()).as("application/json");
    }
}
