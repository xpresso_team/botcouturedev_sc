package ai.commerce.xpresso.v1.controllers;

import ai.commerce.xpresso.v1.utils.ErrorHandler;
import ai.commerce.xpresso.v1.utils.Network;
import com.google.inject.Inject;
import io.swagger.annotations.*;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;


@Api(value = "Utility GET", produces = "application/json", tags= {"utility"})
public class UtilityGet extends Controller {
    @Inject
    private Configuration mConfiguration;

    /**
     * Redirects the request to the logo of requested brand.
     * Brand logo are generated using an offline script which
     * do a google image search and returns top result as most
     * possible brand logo. This image is then stored in s3.
     *
     * Performs following operations
     * 1) If logo exist in S3.
     *    1.1) Return the logo url
     * 2) If logo does not exist in S3.
     *    2.1) Fetch the logo from the google search
     *
     * @return redirection response to new link
     */
    public Result getBrand() {

        final Set<Map.Entry<String, String[]>> entries = request().queryString().entrySet();
        String brandName="";
        for (Map.Entry<String, String[]> entry : entries) {
            if( entry.getKey().equalsIgnoreCase("brand") &&
                    entry.getValue().length > 0){
                brandName = entry.getValue()[0];
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }
        if( brandName.isEmpty()){
            return ok(ErrorHandler
                    .generateError(400,"fail","Brand Name not provided")
                    .toString()).as("application/json");

        }

        String defaultBrandLogoUrl = mConfiguration.getString("datalayer.default_brand_image");
        String redirectUrl = "";
        try {
            redirectUrl = "https://s3.amazonaws.com/xpressobrand/images/" + URLEncoder.encode(brandName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if(Network.checkIfUrlValid(redirectUrl)) {
            return redirect(redirectUrl);
        }else{
            // Do google image search and download
            return redirect(defaultBrandLogoUrl);
        }
    }
}
