package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB;
import ai.commerce.xpresso.v1_1.data.DynamoDBData;
import ai.commerce.xpresso.v1_1.utils.ErrorHandler;
import com.google.inject.Inject;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.*;

/**
 * Created by naveen on 1/5/17.
 *
 * Handles all GET request for the AWS Dynamodb.
 * Uses AWS SDK to create a filtered or normal query to fetch products
 *
 * It has following api:
 *  1) scan All products
 *  2) get single products
 */

public class DynamoDBGet extends Controller {

    @Inject
    private Configuration mConfiguration;

    private AmazonDynamoDB mAmazonDynamoDB = null;

    @Inject
    DynamoDBGet(Configuration configuration){
        mAmazonDynamoDB = new AmazonDynamoDB(configuration);
    }

    /**
     * Performs scan on DynamoDb table using provided filters
     * Returns the complete data with the returned fields
     * @return Results
     */
    public Result scanAll(){

        final Set<Map.Entry<String,String[]>> entries = request().queryString().entrySet();

        int pageSize = mConfiguration.getInt("datalayer.dynamodb.default_page_size");
        int page= mConfiguration.getInt("datalayer.dynamodb.default_page");
        List<String> returnFields = new ArrayList<>();
        // Setting default value for table and key names
        for (Map.Entry<String,String[]> entry : entries) {
            if ( entry.getKey().equalsIgnoreCase("pageSize") &&
                    entry.getValue().length > 0){
                try {
                    pageSize = Integer.parseInt(entry.getValue()[0]);
                }catch(NumberFormatException e){
                    return ok(ErrorHandler
                            .generateError(400, "fail", "Invalid Page size")
                            .toString()).as("application/json");
                }
            }else if(entry.getKey().equalsIgnoreCase("page") &&
                    entry.getValue().length > 0){
                try{
                    page = Integer.parseInt(entry.getValue()[0]);
                }catch(NumberFormatException e){
                    return ok(ErrorHandler
                            .generateError(400, "fail", "Invalid Page No")
                            .toString()).as("application/json");

                }
            }
            else if ( entry.getKey().equalsIgnoreCase("returnField") &&
                    entry.getValue().length > 0) {

                returnFields = Arrays.asList(entry.getValue());
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }

        JSONObject finalResults = new JSONObject();
        JSONArray trimmedResults = new JSONArray();
        try {
            JSONArray results = DynamoDBData.getInstance().getData().getJSONArray("Results");

            int startIndex = page * pageSize;
            int endIndex = (page + 1) * pageSize;
            for( int i=startIndex; i < endIndex && i < results.length(); ++i) {
                if (returnFields.isEmpty()) {
                    trimmedResults.put(results.getJSONObject(i));
                } else {
                    JSONObject trimmedObject = new JSONObject();
                    for( String field: returnFields){
                        // exception to Ignore invalid fields.
                        try {
                            trimmedObject.put(field, results.getJSONObject(i).get(field));
                        }catch (Exception e){
                            Logger.error(e.getMessage());
                        }
                    }
                    trimmedResults.put(trimmedObject);
                }
            }
        } catch (JSONException e) {
            Logger.error(e.getMessage());
        }

        try {
            finalResults.put("code",200);
            finalResults.put("status","ok");
            finalResults.put("msg","ok");
            finalResults.put("ResultsCount",trimmedResults.length());
            finalResults.put("Results",trimmedResults);
        } catch (JSONException e) {
            Logger.error(e.getMessage());
        }
        return ok(finalResults.toString()).as("application/json");
    }


    /**
     * Fetch single product using the primary key eg. xc_sku
     * @return Returns the complete detail of the products
     */
    public Result getProduct(){
        final Set<Map.Entry<String,String[]>> entries = request().queryString().entrySet();
        String keyValue = "";
        // Default key name is xc_sku
        String keyName = mConfiguration.getString("datalayer.dynamodb.default_key_name");
        String tableName = mConfiguration.getString("datalayer.dynamodb.default_table_name");

        for (Map.Entry<String,String[]> entry : entries) {
            if ( entry.getKey().equalsIgnoreCase("keyValue") &&
                    entry.getValue().length > 0){
                keyValue = entry.getValue()[0];
            }else if(entry.getKey().equalsIgnoreCase("keyName") &&
                    entry.getValue().length > 0){
                keyName = entry.getValue()[0];
            }
            else if ( entry.getKey().equalsIgnoreCase("tableName") &&
                    entry.getValue().length > 0) {
                tableName = entry.getValue()[0];
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }

        JSONObject finalResults = new JSONObject();
        try {
            JSONObject result = mAmazonDynamoDB.getProduct(tableName, keyName, keyValue);
            if( result.has("status") &&
                    result.getString("status").equalsIgnoreCase(AmazonDynamoDB.FAIL_STATUS)){
                finalResults = result;
            }else {
                finalResults.put("Results", result);
                finalResults.put("code",200);
                finalResults.put("Status", "ok");
                finalResults.put("Msg", "ok");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ok(finalResults.toString()).as("application/json");
    }
}
