package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearch;
import ai.commerce.xpresso.v1_1.search.SearchItem;
import ai.commerce.xpresso.v1_1.utils.ErrorHandler;
import ai.commerce.xpresso.v1_1.utils.XCJSON;
import com.google.inject.Inject;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.*;

/**
 * Created by naveen on 1/5/17.
 *
 * Handles all GET request for the AWS CLoud Search.
 * Uses AWS SDK to create a structured query and perform search
 *
 * It has following api:
 *  1) Generic API
 */

public class CloudSearchPut extends Controller {
    @Inject
    private Configuration mConfiguration;

    private AmazonCloudSearch mCloudSearch ;

    private String FAIL_STATUS = "fail";

    @Inject
    public CloudSearchPut(Configuration configuration){
        mCloudSearch = new AmazonCloudSearch(configuration);
    }

    /**
     * Performs search on multiple fields in cloud search and
     * return field can contain any fields or complete product details
     *
     * @return Results
     */

    public Result insertItem() {


        JSONObject productData = null;
        try {
            productData= new JSONObject(request().body().asJson().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Check if productData is null
        if(productData == null) {
            return ok(ErrorHandler
                    .generateError(400, "fail", "Invalid Json")
                    .toString()).as("application/json");
        }

        JSONObject finalResult = new JSONObject();
        try {
            if( !productData.has("items") ||
                    productData.getJSONArray("items").length() == 0){

                return ok(ErrorHandler
                        .generateError(400,"fail","Empty items provided")
                        .toString()).as("application/json");
            }else{
                boolean success = mCloudSearch.insertItems(productData.getJSONArray("items"));
                if( success ){
                    finalResult.put("status","ok");
                    finalResult.put("msg","ok");
                    finalResult.put("code",200);
                }else{
                    finalResult = ErrorHandler
                            .generateError(500,"fail","Insert Failed");
                }
            }
        } catch (JSONException e) {
            return ok(ErrorHandler
                    .generateError(400,"fail","Invalid Json")
                    .toString()).as("application/json");
        }

        return ok(finalResult.toString()).as("application/json");
    }
}
