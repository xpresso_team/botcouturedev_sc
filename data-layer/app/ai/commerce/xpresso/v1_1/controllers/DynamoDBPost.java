package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB;
import ai.commerce.xpresso.v1_1.utils.ErrorHandler;
import com.google.inject.Inject;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.*;

/**
 * Created by naveen on 1/5/17.
 *
 * Handles all post request for the AWS Dynamodb.
 * Uses AWS SDK to perform put item request to the database.
 *
 * It supports batch request to the server. By default always batch update is performed.
 *
 * It has following api:
 *  1) update product of an existing data
 */

public class DynamoDBPost extends Controller {
    @Inject
    private Configuration mConfiguration;

    private AmazonDynamoDB mAmazonDynamoDB = null;

    @Inject
    public DynamoDBPost(Configuration configuration){
        mAmazonDynamoDB = new AmazonDynamoDB(configuration);
    }

    /**
     * Fetch single procuct using the primary key eg. xc_sky
     * @return Returns the complete detail of the products
     */
    public Result updateProduct(){

        JSONObject productData = null;
        try {
            productData= new JSONObject(request().body().asJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Check if productData is null
        if(productData == null){
            return ok(ErrorHandler
                    .generateError(400,"fail","Invalid Json")
                    .toString()).as("application/json");
        }


        String tableName = mConfiguration.getString("datalayer.dynamodb.default_table_name");
        String keyName = mConfiguration.getString("datalayer.dynamodb.default_key_name");

        if( productData.has("tableName")){
            try {
                tableName = productData.getString("tableName");
            } catch (JSONException e) {
                return ok(ErrorHandler
                        .generateError(400,"fail","Invalid Json")
                        .toString()).as("application/json");
            }
        }
        if( productData.has("keyName")){

            try {
                keyName = productData.getString("keyName");
            } catch (JSONException e) {
                return ok(ErrorHandler
                        .generateError(400,"fail","Invalid keyName")
                        .toString()).as("application/json");
            }
        }
        String keyValue = "";
        if(! productData.has("keyValue")){
            return ok(ErrorHandler
                    .generateError(400,"fail","keyValue is required")
                    .toString()).as("application/json");

        }

        JSONObject finalResults = new JSONObject();
        try {
            if( productData.has("item")){

                keyValue = productData.getString("keyValue");
                finalResults = mAmazonDynamoDB.updateProduct(
                        tableName,
                        keyName,
                        keyValue,
                        productData.getJSONObject("item"));
            }else{

                return ok(ErrorHandler
                        .generateError(400,"fail","Empty items provided")
                        .toString()).as("application/json");
            }
        } catch (JSONException e) {
            return ok(ErrorHandler
                    .generateError(400,"fail","Invalid Json")
                    .toString()).as("application/json");
        }

        return ok(finalResults.toString()).as("application/json");
    }
}
