package ai.commerce.xpresso.v1_1.amazon.cloudsearch;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.Hit;

import java.util.List;
import java.util.Map;

/**
 * Result of a query executed on Amazon Cloud Search.
 * 
 * @author Tahseen Ur Rehman Fida
 * @email tahseen.ur.rehman@gmail.com
 *
 */
public class AmazonCloudSearchResult {
	public String rid;

	public long time;

	public int found;

	public int start;

	public List<Hit> hits = null;

    public Map<String, List<String>> facets = null;

	// Cursor String
	public String cursor;
}
