/**
 * 
 */
package ai.commerce.xpresso.v1_1.amazon.cloudsearch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.*;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchClientCustom;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


/**
 * @author Koustuv Saha
 * Jun 6, 2016 1:52:44 PM
 * Synaptica  Domain
 */
public class XCDomain {

	int index;
	String domainName;
	String tableName;
	String endPoint;
	String suggesterEndPoint;
	String s3Url;
	String searchReturnAttributes;

	ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchClientCustom cloudSearchClient;
	//	private static final Map<String, List<String>> categoriesMap = new LinkedHashMap<String, List<String>>();
	private Map<String, List<String>> categoriesMap;

	public XCDomain(JSONObject domainObj, int index) {

		this.index = index;
		this.categoriesMap = new LinkedHashMap<String, List<String>>();
		try {
			this.domainName = domainObj.getString("Domain Name").toLowerCase().trim();
			this.tableName = domainObj.getString("Table Name").trim();
			this.endPoint = domainObj.getString("Endpoint");
			this.suggesterEndPoint = domainObj.has("Suggester Endpoint") ? domainObj.getString("Suggester Endpoint") : "";
			this.s3Url = domainObj.has("S3 URL") ? domainObj.getString("S3 URL") : "";

			//			this.suggesterEndPoint = domainObj.getString("Suggester Endpoint");
			//			this.s3Url = domainObj.getString("S3 URL");
			this.searchReturnAttributes = domainObj.getString("Return Attributes");
			JSONObject mapObj = domainObj.getJSONObject("Mappings");

			Iterator<?> granularCategories = mapObj.keys();
			while (granularCategories.hasNext()) {
				//				String currentCategory = ((String) granularCategories.next()).toLowerCase().trim();
				String currentCategory = ((String) granularCategories.next());
				JSONArray mappedCatg = mapObj.getJSONArray(currentCategory);
				List<String> mapSet = new ArrayList<String>();
				for (int i = 0; i < mappedCatg.length(); i++) {
					mapSet.add(mappedCatg.getString(i).toLowerCase().trim());
				}
				categoriesMap.put(currentCategory, mapSet);
			}
			//			domainOntologyMap.put(domainName, categoriesMap);
			//			domainTableMap.put(domainName, tableName);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		this.setCloudSearchClient();

	}

	public List<String> getMappedCategories(String granularCategory) {
		return categoriesMap.get(granularCategory);
	}

	/**
	 * Returns whether the granularCategory belongs to permissible categories in this domain.
	 * @param granularCategory
	 * @return
	 */
	public boolean isCategoryOfDomain(String granularCategory) {
		return categoriesMap.containsKey(granularCategory);
	}

	/**
	 * @return the cloudSearchClient
	 */
	public ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchClientCustom getCloudSearchClient() {
		return cloudSearchClient;
	}

	private void setCloudSearchClient() {
		this.cloudSearchClient = new ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchClientCustom(ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig.profileCredentials, ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig.configuration);
		this.cloudSearchClient.setRegion(ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig.US_EAST1);
		this.cloudSearchClient.setDocumentEndpoint("doc-" + this.endPoint + ".us-east-1.cloudsearch.amazonaws.com");
		this.cloudSearchClient.setSearchEndpoint("search-" + this.endPoint + ".us-east-1.cloudsearch.amazonaws.com");
	}

	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @return the searchEndPoint
	 */
	public String getEndPoint() {
		return endPoint;
	}

	/**
	 * @return the suggesterEndPoint
	 */
	public String getSuggesterEndPoint() {
		return suggesterEndPoint;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return the s3Url
	 */
	public String getS3Url() {
		return s3Url;
	}

	public String getSearchReturnAttributes() {
		return searchReturnAttributes;
	}
}
