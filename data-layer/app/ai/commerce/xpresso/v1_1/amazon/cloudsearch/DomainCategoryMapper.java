package ai.commerce.xpresso.v1_1.amazon.cloudsearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchClientCustom;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain;
import ai.commerce.xpresso.v1_1.utils.XCLogger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;


public class DomainCategoryMapper {

	private static final Map<String, List<String>> attributeCategoriesMap = new LinkedHashMap<String, List<String>>();

	//	private static final Map<String, Map<String, List<String>>> domainOntologyMap = new LinkedHashMap<String, Map<String, List<String>>>();
	//	private static final Map<String, String> domainTableMap = new HashMap<String, String>();

	private static final Map<String, Integer> domainIdxMap = new HashMap<String, Integer>();
	public static ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain[] domainObjectsArr;

	public static Logger logger = ai.commerce.xpresso.v1_1.utils.XCLogger.getSpLogger();

	private static ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain getDomainObject(String domain) {
		if (domain == null)
			return null;
		int index = domainIdxMap.get(domain);
		return domainObjectsArr[index];
	}

	/**
	 * The method retrieves the domain-specific subcategories corresponding to the granular
	 * subcategory
	 * @param domain
	 * @param granularCatg
	 * @return
	 */
	public static List<String> getMappedCategories(String domain, String granularCatg) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null)
			return null;
		return domainObj.getMappedCategories(granularCatg);
	}

	/**
	 * Checks whether the granular category belongs to the domain.
	 * @param domain
	 * @param granularCatg
	 * @return
	 */
	public static boolean isCategoryOfDomain(String domain, String granularCatg) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null)
			return false;
		return domainObj.isCategoryOfDomain(granularCatg);
	}

	public static String getTableName(String domain) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getTableName();
	}

	public static String getEndPoint(String domain) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getEndPoint();
	}

	public static String getSuggesterEndPoint(String domain) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getSuggesterEndPoint();
	}

	public static ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchClientCustom getAmazonCloudSearchClient(String domain) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain domainObj = getDomainObject(domain);
		return domainObj.getCloudSearchClient();
	}

	public static ArrayList<String> getAllSubCategories() {
		List<String> allCategories = new ArrayList<>();
		ArrayList<String> allSubCategories = new ArrayList<>();
		for (List<String> categoriesMappedToOneAttribute : attributeCategoriesMap.values())
			allCategories.addAll(categoriesMappedToOneAttribute);
		for (String domain : domainIdxMap.keySet())
			for (String category : allCategories) {
				if (getMappedCategories(domain, category) != null) {
					allSubCategories.addAll(getMappedCategories(domain, category));
					//					System.out.println(getMappedCategories(domain, category));
				}

			}
		allSubCategories.addAll(allCategories);
		//		System.out.println(allSubCategories);
		return allSubCategories;
	}

	public static void main(String[] args) {
		logger.info(getMappedCategories("iconic", "dress").toString());
	}

	public static String getS3Url(String domainName) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCDomain domainObj = getDomainObject(domainName);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getS3Url();
	}

}
