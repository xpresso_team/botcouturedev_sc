package ai.commerce.xpresso.v1_1.data;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;

/**
 * Created by naveen on 11/5/17.
 *
 * Singleton
 * Stores intermediately data from dynamoDB.
 * Every update to dynamodb happens here also.
 */
public class DynamoDBData {

    private static DynamoDBData mInstance = null;

    private JSONObject mDynamoDBData = new JSONObject(  );

    private String mTableName = new String();
    private ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB mAmazonDynamoDB = null;

    public static DynamoDBData getInstance(Configuration configuration){
        if( mInstance == null){
            mInstance = new DynamoDBData(configuration);
        }
        return mInstance;
    }

    public static DynamoDBData getInstance(){
        return mInstance;
    }

    private DynamoDBData(Configuration configuration){
        mAmazonDynamoDB = new ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB(configuration);
        ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig.maxReviewCount = 10;
    }

    public void setTable(String tableName){
        mTableName = tableName;
    }

    public void init(){
        long startTime = System.currentTimeMillis();
        long endTime = System.currentTimeMillis();
        play.Logger.info("Time taken:["+(endTime - startTime)+" milli second]");
    }

    public JSONObject getData(){
        return mDynamoDBData;
    }
}