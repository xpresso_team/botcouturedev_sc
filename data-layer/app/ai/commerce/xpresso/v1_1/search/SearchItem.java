package ai.commerce.xpresso.v1_1.search;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.Hit;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1_1.search.SearchUtils;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.*;
import java.util.Map.Entry;

/**
 * The class contains the results of search on the Amazon CloudSearch
 * @author antarip.biswas
 *
 */
public class SearchItem {

	/**
	 * score is the search relevance score obtained from the CloudSearch
	 */

	private Map<String, String> attributeMap = new LinkedHashMap<String, String>();

	public SearchItem(Hit amazonSearchResult) {
		Map<String, String> currentItemMap = amazonSearchResult.fields;

		for (Entry<String, String> entry : currentItemMap.entrySet()) {

			String keyStr = entry.getKey();
			String valueStr = entry.getValue();
            if( XCConfig.ENABLE_STANDARD_SIZE &&
                    keyStr.equalsIgnoreCase("standard_size")){
                this.attributeMap.put("size", valueStr);
            }else {
                this.attributeMap.put(keyStr, valueStr);
            }
		}
	}

	public SearchItem(Map<?, ?> dynamoDBSearchResult) {

		for (Entry<?, ?> entry : dynamoDBSearchResult.entrySet()) {
			String keyStr = (String) entry.getKey();
			String valueStr = ((AttributeValue) entry.getValue()).getS();

			//			String mappedKeyStr = SearchUtils.getMappedAttribute(keyStr);
			//			if (mappedKeyStr == null) {
			//				mappedKeyStr = keyStr;
			//			}
			//			this.attributeMap.put(mappedKeyStr, valueStr);
			this.attributeMap.put(keyStr, valueStr);
		}
	}

	public SearchItem() {
		// TODO Auto-generated constructor stub
	}

	public List<ai.commerce.xpresso.v1_1.search.SearchItem> getSearchItemList(List<?> resultList, Boolean isSuggested) {
		List<ai.commerce.xpresso.v1_1.search.SearchItem> searchItemList = new ArrayList<ai.commerce.xpresso.v1_1.search.SearchItem>();
		getSearchItemList(null, searchItemList, 100, isSuggested, resultList);
		return searchItemList;
	}

	public static void getSearchItemList(Map<String, Double> skuConfidenceMap, List<ai.commerce.xpresso.v1_1.search.SearchItem> resultSearchItemList, double confidenceFactor, Boolean isSuggested, List<?> resultList) {
		double netConfidence = confidenceFactor;
		if (resultList != null) {
			for (Object currResult : resultList) {
				ai.commerce.xpresso.v1_1.search.SearchItem item = null;
				if (currResult instanceof Hit) {
					item = new ai.commerce.xpresso.v1_1.search.SearchItem((Hit) currResult);
					//					System.out.println("Current Score: " + item.getAttributeValue(SearchUtils.SCORE_KEY));
					netConfidence = Double.parseDouble(item.getAttributeValue(ai.commerce.xpresso.v1_1.search.SearchUtils.SCORE_KEY)) * confidenceFactor / 100;
				} else if (currResult instanceof Map<?, ?>) {
					item = new ai.commerce.xpresso.v1_1.search.SearchItem((Map<?, ?>) currResult);
				}
				if (item != null) {
					String sku = item.getAttributeValue(ai.commerce.xpresso.v1_1.search.SearchUtils.SKU_KEY);
					item.addAttribute(ai.commerce.xpresso.v1_1.search.SearchUtils.CONFIDENCE_KEY, String.valueOf(netConfidence));
					item.addAttribute(ai.commerce.xpresso.v1_1.search.SearchUtils.SUGGESTED_KEY, String.valueOf(isSuggested));

					if (sku != null && skuConfidenceMap != null) {
						Double existingConfidence = skuConfidenceMap.get(sku);
						if (existingConfidence == null || existingConfidence < netConfidence) {
							resultSearchItemList.add(item);
							skuConfidenceMap.put(sku, netConfidence);
						}
					} else {
						resultSearchItemList.add(item);
					}
				}
			}

		}
	}

	public void assignConfidence(double confidenceFactor) {
		double currConfidence = Double.parseDouble(this.getAttributeValue(ai.commerce.xpresso.v1_1.search.SearchUtils.CONFIDENCE_KEY));
		double confidence = currConfidence * confidenceFactor / 100;
		this.addAttribute(SearchUtils.CONFIDENCE_KEY, String.valueOf(confidence));

		//		System.out.println("Color: " + this.getAttributeValue(SearchUtils.COLOR_KEY) + "\tInitial confidence: " + currConfidence + "\tFinal Confidence: " + confidence);
	}

	public String getAttributeValue(String attribute) {
		return this.attributeMap.get(attribute);
	}

	public void addAttribute(String attribute, String value) {
		this.attributeMap.put(attribute, value);
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SearchItem [attributeMap=" + attributeMap + "]";
	}

    public JSONObject getJSONObject() {
        JSONObject currentJson = new JSONObject();
        Map<String, String> attributeMap = getAttributeMap();

        try {

            for (Entry<String, String> entry : attributeMap.entrySet()) {

                // This means it is an array. Convert to array
                if (entry.getValue().startsWith("[") && entry.getValue().endsWith("]")) {
                    String[] values= entry.getValue()
                            .substring(1, entry.getValue().length() - 1)
                            .replace("\"", "")
                            .split(",");
                    currentJson.put(entry.getKey(), Arrays.asList(values));

                } else {
                    currentJson.put(entry.getKey(), entry.getValue());

                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }

        return currentJson;
    }

}
