package ai.commerce.xpresso.v1_1.utils;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by naveen on 15/5/17.
 */
public class Network {
    public static boolean checkIfUrlValid(String URLName){
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
