name := "datalayer"

version := "2.0.0"

organization := "abzooba"

lazy val `datalayer` = (project in file("."))
  .enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc ,
  cache ,
  javaWs
)

libraryDependencies ++= Seq(
  "com.amazonaws" % "aws-java-sdk" % "1.11.125" ,
  "org.codehaus.jettison" % "jettison" % "1.3.3" ,
  "org.apache.httpcomponents" % "fluent-hc" % "4.5",
  "io.swagger" % "swagger-play2_2.11" % "1.5.3",
  "mysql" % "mysql-connector-java" % "5.1.18",
  "redis.clients" % "jedis" % "2.9.0",
  "com.mashape.unirest" % "unirest-java" % "1.4.9",
  "org.apache.httpcomponents" % "httpclient" % "4.3.6",
  "org.apache.httpcomponents" % "httpasyncclient" % "4.0.2",
  "org.apache.httpcomponents" % "httpmime" % "4.3.6",
  "org.json" % "json" % "20140107"
)

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "MvnRepository" at "https://mvnrepository.com/artifact"
