#! /bin/bash
set -e

if [ "$1" = 'test' ]; then
    if [ -f ./target/universal/stage/RUNNING_PID ]; then
        rm ./target/universal/stage/RUNNING_PID
    fi
    target/universal/stage/bin/datalayer -Dhttp.port=9001 -Dconfig.file=conf/applicationprod.conf &
    sleep 60
    exec /datalayer/deploy/integration_test.sh
elif [ "$1" = 'experimental' ]; then
    if [ -f ./target/universal/stage/RUNNING_PID ]; then
        rm ./target/universal/stage/RUNNING_PID
    fi
    target/universal/stage/bin/datalayer -Dhttp.port=9001 -Dconfig.file=conf/application.conf
elif [ "$1" = 'poc' ]; then
    if [ -f ./target/universal/stage/RUNNING_PID ]; then
        rm ./target/universal/stage/RUNNING_PID
    fi
    target/universal/stage/bin/datalayer -Dhttp.port=9001 -Dconfig.file=conf/applicationpoc.conf
elif [ "$1" = 'production' ]; then
    if [ -f ./target/universal/stage/RUNNING_PID ]; then
        rm ./target/universal/stage/RUNNING_PID
    fi
    target/universal/stage/bin/datalayer -Dhttp.port=9001 -Dconfig.file=conf/applicationprod.conf
else
    exec "$@"
fi

